---
title: "Exemple de rapport automatisé"
author: ''
date: 'Date de création du rapport: `r Sys.Date()`'
vignette: "%\\VignetteIndexEntry{Exemple de rapport} %\\VignetteEngine{knitr::rmarkdown}
  %\\VignetteEncoding{UTF-8}\n"
resources:
- name: img
  files: img/
params:
  nom_referentiel_indications: "lst_ref_fichcomp_aap_aac_medatu_082024.csv"
  ucd_codes_list:
   - "3400894197232"
  nom_medicament: "Keytruda"
  code_indication: "CPEMB03"
  start_date: "2022-04-12"
  end_date: "2022-08-26"
  estimations_supplementaires: 
  - "IND_MAX"
  indications_multiples: true
output:
  pdf_document: 
    extra_dependencies: ["float"]
  html_document: 
    bookdown::html_document2: 
      base_format: rmarkdown::html_vignette
link-citations: true
---

---
title: "`r params$nom_medicament`, code indication : `r params$code_indication`, période: `r params$start_date` au `r params$end_date`"
---


---
subtitle: "Codes UCD : `r params$ucd_codes_list`"
---

```{r, echo=FALSE}
knitr::opts_chunk$set(
  warning = FALSE,
  echo = FALSE,
  collapse = TRUE,
  comment = "#>",
  message = FALSE,
  fig.pos = "H", # for position of figure in pdf
  out.extra = "" # for position of figure in pdf
)

# paramètre d'affichage de l'année de la cartographie dans le texte du rapport
annee_carto <- 2022
```

```{r, echo=FALSE}
# Ce rapport est obtenu en renseignant les paramètres comme
# indiqué dans la vignette de prise en main appelée `vignette("rapac")`.
library(openxlsx)
library(knitr)
library(ggplot2)
library(dplyr)
library(lubridate) # for date objects
library(arrow) # write compressed tables
library(stringr)
library(ggrepel)

is_package <- require(rapac)
if (!is_package) {
  source("../R/constants.R")
  source("../R/rapport_utils.R")
  source("../R/extraction_utils.R")
}

# Vérifie la cohérence du nom du médicament et du code indication
indications <- get_indications(params$nom_referentiel_indications)

if (!is.null(params$nom_medicament)) {
  indications_medicament <- indications |>
    filter(
      stringr::str_detect(libelle, stringr::regex(params$nom_medicament,
        ignore_case = TRUE
      ))
    )
} else {
  stop("Le nom du médicament n'est pas renseigné.")
}

if (nrow(indications_medicament) == 0) {
  stop("Le médicament demandé n'est pas dans le référentiel ATIH fourni.
  Veuillez fournir un nom de médicament existant.")
}

if (params$code_indication %in% (indications_medicament %>% pull(indication))) {
  indication_label <- indications |>
    filter(indication == params$code_indication) |>
    select(lib_indication) |>
    pull()
} else {
  stop("Le code indication demandé n'est pas dans le référentiel ATIH fourni.
  Veuillez fournir un code indication existant.")
}

if (!is.null(params$ucd_codes_list)) {
  ucd_codes_list_in_indications <- intersect(
    params$ucd_codes_list,
    indications_medicament %>% pull(ucd13)
  )

  # On doit retrouver au moins un code ucd demandé par l'utilisateur dans le
  # référentiel (pour ce médicament et cette indication).
  if (length(ucd_codes_list_in_indications) == 0) {
    stop("Les code UCD demandés ne sont pas retrouvés pour le médicament et
    l'indication demandés. Veuillez fournir des code UCD parmi ceux du
    médicament et de l'indication demandés.")
  }
  # les codes ucd demandés doivent être inclus dans ceux existant pour le
  # médicament et l'indication demandée
  if (length(ucd_codes_list_in_indications) != length(params$ucd_codes_list)) {
    stop("Un des codes UCD demandés n'est pas retrouvé parmi les codes UCD du
    médicament et de l'indication demandé. Veuillez fournir des code UCD
    uniquement parmi ceux du médicament et de l'indication demandés.")
  }
}


start_date <- as.Date(params$start_date)
end_date <- as.Date(params$end_date)
estimations <- c("TOT_DCIR", "IND_BAS", "IND_HAUT")
if (!is.null(params$estimations_supplementaires)) {
  estimations <- c(estimations, params$estimations_supplementaires)
}

# rapac constants (file path and global variables)
constants <- Constants_rapac()
```

Dispensation du médicament `r params$nom_medicament` (codes UCD: `r params$ucd_codes_list`) sur la période `r params$start_date` 
au `r params$end_date` pour l'indication *`r params$code_indication` : `r indication_label`*.

NB: Une dispensation correspond à la délivrance du médicament à un patient à une
date donnée, quelque soit la dose du médicament.

```{r echo=FALSE}
# Création d'une table de données agrégées `aggregated_results.xlsx`.
# Cette partie ne fonctionne que sur le portail CNAM..

if (constants$IS_PORTAIL) {
  library(ROracle) # charger le package Roracle
  library(dbplyr)

  Sys.setenv(TZ = "Europe/Paris") # fuseaux horaires
  Sys.setenv(ORA_SDTZ = "Europe/Paris")
  drv <- dbDriver("Oracle") # pilote pour se connecter à une base oracle
  conn <- dbConnect(drv, dbname = "IPIAMPR2.WORLD") # connecte à la base
  # requête SNDS des données patients individuelles
  extraction_data <- extract_acces_precoces(
    conn = conn,
    drug_name = params$nom_medicament,
    code_indication = params$code_indication,
    start_date = start_date,
    end_date = end_date,
    nom_referentiel_indications = params$nom_referentiel_indications,
    ucd_codes_list = params$ucd_codes_list
  )
  consos <- extraction_data[["consommations"]]
  patients <- extraction_data[["patients"]]
  events <- extraction_data[["events"]]
  ## Jusqu'ici les données comportent des doublons, notamment entre les
  # sources ER_UCD_F et FH ou FHSTC.

  # Création du set de données agrégées pour le rapport
  create_rapport_data(
    drug_name = params$nom_medicament,
    start_date = start_date,
    end_date = end_date,
    code_indication = params$code_indication,
    reference_filename = params$nom_referentiel_indications,
    ucd_codes_list = params$ucd_codes_list,
    indications_multiples = params$indications_multiples,
    private = TRUE
  )
}

path2study <- get_path2study(
  params$nom_medicament,
  params$code_indication,
  start_date,
  end_date,
  pkg_data = !constants$IS_PORTAIL
)
workbook <- loadWorkbook(
  file.path(path2study, paste0("private_", constants$FILENAME_AGG_RESULTS))
)

# La suite du programme produit le rapport pdf. Elle peut être réalisée en
# local, après avoir exporté le fichier `aggregated_results.xlsx`. Dans ce cas
# le rapport sera disponible en local. Une autre possibilité est de l'exécuter
# sur le portail, puis d'exporter directement le rapport pdf.
```

**Remarque sur les petits effectifs:** Les petits effectifs (<10) sont masqués
pour des raisons de confidentialité.  Les totaux sont en revanche préservés. Par
conséquent, les totaux peuvent ne pas correspondre à la somme des effectifs
affichés.

# Nombres de patients et de dispensations concernés

## Nombres de patients et de dispensations par mois

Plusieurs estimations sont présentées (cf. annexe en fin de document pour les
détails sur les sources de données utilisées): 

`r if("TOT_DCIR" %in% estimations){"- *TOT_DCIR* : Cette estimation prend en
compte toutes les dispensations du médicament cible, peu importe l'indication. A
noter que dans le cas où le médicament a plusieurs indications, cette estimation
peut être très supérieure à celle portant uniquement sur l'indication demandée
(IND_BAS)."}`

`r if("TOT_PMSI" %in% estimations){"- *TOT_PMSI* : Cette estimation prend en
compte les dispensations du médicament cible en hospitalisation toutes
indications confondues et en rétrocession dans un établissement public toutes
indications confondues (rétrocessions dans un établissement privé non prises en
compte)."}`

`r if("IND_BAS" %in% estimations){"- *IND_BAS* : Cette estimation dite « basse
» prend en compte uniquement les dispensations du médicament cible dans
l'indication demandée (rétrocessions dans un établissement privé non prises en
compte)."}`

`r if("IND_HAUT" %in% estimations){"- *IND_HAUT* : Cette estimation dite « haute
» prend en en compte les dispensations du médicament cible dans l'indication
demandée et dans les autres indications du médicament inconnues du référentiel.
En d'autres termes, cette estimation exclut les dispensations dans toutes les
indications connues du référentiel autres que l'indication demandée
(rétrocessions dans un établissement privé non prises en compte)."}`

`r if("IND_MAX" %in% estimations){"- *IND_MAX* : Cette estimation prend en
compte les dispensations du médicament cible dans l'indication demandée et dans
les indications inconnues du référentiel (idem IND_HAUT), ainsi que les
rétrocessions dans un établissement privé (toutes indications)."}`

### Estimations du nombre de patients distincts et de dispensations du médicament `r params$nom_medicament` par source de données sur la période `r start_date` / `r end_date`.

```{r}
summary_sources <- read.xlsx(workbook, sheet = constants$WS_SOURCES)
colnames(summary_sources) <- gsub("\\.", " ", colnames(summary_sources))

estimation_regex <- paste0(estimations, collapse = "|")

kable(
  summary_sources |>
    filter(grepl(estimation_regex, source)) |>
    mutate(
      source = trimws(gsub(" \\(.*?\\)", "", source))
    ),
  caption = "Nombre de patients distincts et de dispensations par source de données",
  digits = 1,
  align = "l",
  table.attr = "style='width:100%;'"
)
```

### Graphiques des nombres de patients distincts et de dispensations du médicament `r params$nom_medicament` par mois sur la période `r start_date` / `r end_date`. 

```{r nb_patients, width = 15, height = 10, fig.cap="Nombre de patients distincts par mois"}
LABEL_EST <- "Estimations"
# transforme en facteurs avec un ordre donné pour imposer l'ordre d'affichage dans les graphiques.
consos_per_month <- read.xlsx(workbook, sheet = constants$WS_CONSOS_MOIS) |>
  rename(
    !!LABEL_EST := estimation
  ) |>
  filter(grepl(estimation_regex, Estimations)) |>
  dplyr::mutate(!!sym(LABEL_EST) := factor(!!sym(LABEL_EST), levels = c("IND_BAS", "IND_MAX", "IND_HAUT", "TOT_PMSI", "TOT_DCIR"))) |>
  dplyr::arrange(LABEL_EST, month_year)

colnames(consos_per_month) <- gsub("\\.", " ", colnames(consos_per_month))


PLOT_TEXT_SIZE <- 16
L_WIDTH <- 1.5
POINT_SIZE <- 4
LABEL_SIZE <- 4
LEGEND_SIZE <- 11
custom_palette <- c("#7a7ac8", "#db9539")
margin_factor <- 20

# patient plots
max_patients <- max(consos_per_month |> pull(constants$LABEL_NB_PATIENTS))
margin <- max_patients / margin_factor
ggplot(
  consos_per_month,
  aes(
    x = month_year,
    y = !!sym(constants$LABEL_NB_PATIENTS),
    group = !!sym(LABEL_EST),
    colour = !!sym(LABEL_EST)
  )
) +
  geom_line(linewidth = L_WIDTH) +
  geom_point(size = POINT_SIZE) +
  labs(
    x = "Année-Mois",
    y = "Nombre de patients"
  ) +
  geom_text_repel(
    aes(y = !!sym(constants$LABEL_NB_PATIENTS), label = !!sym(constants$LABEL_NB_PATIENTS)),
    size = LABEL_SIZE, color = "black", max.overlaps = 5, force_pull = 5, nudge_y = 2, nudge_x = 0.01
  ) +
  scale_fill_manual(values = custom_palette[2], name = "Variable") +
  theme_minimal(base_size = PLOT_TEXT_SIZE) +
  theme(
    axis.text.x = element_text(angle = 45, hjust = 1),
    legend.position = c(0.15, 0.8), # "top",
    legend.text = element_text(size = LEGEND_SIZE),
    axis.title.y = element_text(colour = custom_palette[2])
  ) +
  ylim(0, max_patients + margin)
```

```{r nb_dispensations, width = 15, height = 10, fig.cap="Nombre de dispensations par mois"}
# Delivrance plots
max_delivrances <- max(consos_per_month |> pull(constants$LABEL_DISPENSATIONS))
margin <- max_delivrances / margin_factor
ggplot(
  consos_per_month,
  aes(
    x = month_year,
    y = !!sym(constants$LABEL_DISPENSATIONS),
    group = !!sym(LABEL_EST),
    colour = !!sym(LABEL_EST)
  )
) +
  geom_line(linewidth = L_WIDTH) +
  geom_point(size = POINT_SIZE) +
  labs(
    x = "Année-Mois",
    y = "Nombre de dispensations"
  ) +
  geom_text_repel(aes(y = !!sym(constants$LABEL_DISPENSATIONS), label = !!sym(constants$LABEL_DISPENSATIONS)),
    size = LABEL_SIZE, color = "black", max.overlaps = 5, force_pull = 5, nudge_y = 2, nudge_x = 0.01
  ) +
  theme_minimal(base_size = PLOT_TEXT_SIZE) +
  theme(
    axis.text.x = element_text(angle = 45, hjust = 1),
    legend.position = c(0.15, 0.8), # "top",
    legend.text = element_text(size = LEGEND_SIZE),
    axis.title.y = element_text(colour = custom_palette[1])
  ) +
  ylim(0, max_delivrances + margin)
```

# Caractéristiques de la population 

```{r}
estimation_ttes_indications <- summary_sources |>
  filter(source == constants$LABEL_SHORT_EST_TOT_DCIR) |>
  pull(constants$LABEL_NB_PATIENTS)
estimation_indication <- summary_sources |>
  filter(grepl(constants$LABEL_SHORT_EST_IND_BAS, source)) |>
  pull(constants$LABEL_NB_PATIENTS)
```

`r if(!params$indications_multiples){"Comme indiqué par l'utilisateur, le
médicament dispose d'une autorisation d'accès précoce dans une seule indication
: les données restituées sur la population proviennent de l'estimation
TOT_DCIR."}`

`r if(params$indications_multiples){"Comme indiqué par l'utilisateur, le
médicament dispose d'une autorisation d'accès précoce dans plusieurs indications
: les données restituées sur la population proviennent de l'estimation
IND_BAS."}`

## Circuit de dispensations 

```{r}
summary_type_delivrance <- read.xlsx(workbook, sheet = constants$WS_TYPE)
colnames(summary_type_delivrance) <- gsub("\\.", " ", colnames(summary_type_delivrance))

kable(
  summary_type_delivrance,
  caption = "Nombre de patients et de dispensations par
  type de circuit. Les petits effectifs (<10) sont masqués
pour des raisons de confidentialité.",
  digits = 1,
  align = "l",
  table.attr = "style='width:100%;'"
)
```

## Catégorie d'établissements

Les chiffres suivants présentent le nombre d'initiation de traitement par
catégorie d'établissements. Une initiation correspond à la première dispensation
sur la période étudiée pour chaque patient. 

```{r nb_initiations_ete_cat}
summary_ete_cat <- read.xlsx(workbook, sheet = constants$WS_ETE_CAT)
colnames(summary_ete_cat) <- gsub("\\.", " ", colnames(summary_ete_cat))

kable(
  summary_ete_cat,
  caption = "Catégorie des établissements initiant le
  traitement. Les petits effectifs (<10) sont masqués
pour des raisons de confidentialité.",
  digits = 1,
  align = "l",
  table.attr = "style='width:100%;'"
)
nb_patients_estimation_retenue <- summary_ete_cat |>
  pull(constants$LABEL_INIT) |>
  sum()
```

## Caractéristiques démographiques des patients

L'âge est donné en années au moment de la première dispensation du médicament en
accès précoce.

```{r pyramid_plot, fig.cap="Pyramide des âges. \n Les petits effectifs (<10) sont masqués pour des raisons de confidentialité."}
library(stringr)
# Plot
age_sex_counts <- read.xlsx(workbook, sheet = constants$WS_AGE_SEXE) |>
  mutate(
    n = case_when(
      sex_normalized == "Masculin" ~ -nb_patients,
      TRUE ~ nb_patients
    ),
    n_label = ifelse(n < 0, -n, n)
  )
# Make the age_sex_counts for males negative for the pyramid plot
ggplot(age_sex_counts, aes(x = age_group, y = n, fill = sex_normalized)) +
  geom_bar(stat = "identity") +
  scale_y_continuous(labels = abs) + # Make y-axis labels positive
  scale_fill_manual(values = c("#515cc2", "#db9539"), name = "Sexe") +
  labs(x = "Age", y = constants$LABEL_NB_PATIENTS, fill = "Sex") +
  geom_text(aes(label = n_label), hjust = -0.5) +
  coord_flip() + # Flip coordinates for a horizontal pyramid
  theme_minimal(base_size = PLOT_TEXT_SIZE) +
  theme(
    legend.position = c(1, 0), # Place legend at the extreme bottom-right
    legend.justification = c(1, 0) # Justify the legend to the bottom-right
  )
```


## Comorbidités

### Affections longue durée (ALD) 

NB: un patient peut avoir plusieurs ALD en cours. Les codes CIM-10 des ALD sont
regroupés par famille de diagnostics (sur les trois premiers caractères du code CIM-10).

```{r alds, fig.cap="Proportion des patients avec une ALD (pour les ALD avec prévalence >=1%)"}
summary_ald <- read.xlsx(workbook, sheet = constants$WS_ALD) |>
  mutate(
    Prevalence = round(100 * Nombre.de.patients / nb_patients_estimation_retenue, 1)
  ) |>
  rename("ALD" = "IR_IMB_R") |>
  filter(Prevalence >= 1)
colnames(summary_ald) <- gsub("\\.", " ", colnames(summary_ald))

summary_ald$ALD <- factor(summary_ald$ALD, levels = rev(summary_ald$ALD))
ggplot(summary_ald, aes(x = Prevalence, y = ALD, fill = ALD)) +
  geom_bar(stat = "identity", color = "black") +
  labs(
    x = "Proportion des patients (%)", y = NULL
  ) +
  scale_x_continuous(breaks = c(0, 25, 50, 75, 100), limits = c(0, 108)) +
  theme_minimal(base_size = 15) +
  theme(legend.position = "none") +
  geom_text(aes(label = Prevalence, x = Prevalence + 1), hjust = 0)
```

### Prévalence des pathologies sur l'année `r annee_carto` (cartographie CNAM)

```{r pathos, fig.cap="Proportion des patients pour chaque pathologie"}
label2code_pathos <- setNames(
  names(constants$TOP_PATHOS_TOP_LVL),
  constants$TOP_PATHOS_TOP_LVL
)
label2code_cancers <- setNames(
  names(constants$TOP_PATHOS_CANCER),
  constants$TOP_PATHOS_CANCER
)
top_pathos_means <- read.xlsx(workbook, sheet = constants$WS_TOP_PATHOS) |>
  rename(all_of(c(label2code_pathos, label2code_cancers))) |>
  round(1)

data_long <- tidyr::gather(
  top_pathos_means[names(label2code_pathos)],
  key = "Condition",
  value = "Prevalence"
)
data_long$Condition <- factor(
  data_long$Condition,
  levels = rev(names(label2code_pathos))
)
# Create a horizontal barplot using ggplot2
ggplot(data_long, aes(x = Prevalence, y = Condition, fill = Condition)) +
  geom_bar(stat = "identity", color = "black") +
  labs(
    x = "Proportion des patients (%)", y = NULL
  ) +
  scale_x_continuous(breaks = c(0, 25, 50, 75, 100), limits = c(0, 108)) +
  theme_minimal(base_size = 15) +
  theme(legend.position = "none") +
  geom_text(aes(label = Prevalence, x = Prevalence + 1), hjust = 0)
```

La méthodologie utilisée pour identifier ces pathologies est décrite dans
[ici](https://www.assurance-maladie.ameli.fr/etudes-et-donnees/par-theme/pathologies/cartographie-assurance-maladie). 


### Prévalence des cancers sur l'année `r annee_carto` (cartographie CNAM)

```{r cancers, fig.cap="Proportion des patients avec un cancer"}
data_long_cancers <- tidyr::gather(
  top_pathos_means[names(label2code_cancers)],
  key = "Condition", value = "Prevalence"
)
data_long_cancers$Condition <- factor(
  data_long_cancers$Condition,
  levels = rev(names(label2code_cancers))
)
cancer_palette <- c(
  "#fdfdec", "#c5bc88", "#817662",
  "#7ec77e", "#02a802", "#006400",
  "#6a87dd", "#4a4ac5", "#000080",
  "#f8c15b", "#faa741", "#ce8416",
  "#FFB6C1", "#ff69B4", "#ff1493"
)
# Si la table data_long_cancers est vide ou manque un cancer, on ajoute une ligne avec prévalence 0
# Cela permet d'éviter un bug lorsqu'on ajoute les couleurs
for (lvl in levels(data_long_cancers$Condition)){
  if (!(lvl %in% data_long_cancers$Condition)){
  new_row <- data.frame(
    Prevalence = 0,
    Condition = lvl 
  )
  # Combine with the original dataframe
  data_long_cancers <- rbind(data_long_cancers, new_row)  
  }
}
data_long_cancers$colour_cancer <- cancer_palette
ggplot(
  data_long_cancers,
  aes(x = Prevalence, y = Condition, fill = Condition)
) +
  geom_bar(stat = "identity", color = "black") +
  labs(
    x = "Prévalence (%)", y = NULL
  ) +
  theme_minimal(base_size = 15) +
  theme(
    legend.position = "none",
    axis.text.y = element_text(colour = data_long_cancers$colour_cancer),
  ) +
  xlab("Proportion des patients (%)") +
  scale_x_continuous(breaks = c(0, 25, 50, 75, 100), limits = c(0, 108)) +
  scale_fill_manual(values = cancer_palette) +
  geom_text(aes(label = Prevalence, x = Prevalence + 1), hjust = 0)
```

## Annexe

### Nombre de patients estimés selon les sources d'information traitées

Toutes les estimations calculées sont présentées ci-dessous. Celles-ci utilisent des
sources de données différentes pour estimer le nombre de patients et de
dispensation : 

- IND_BAS :
une estimation basse avec l'indication d'intérêt demandée par l'utilisateur. Les
données proviennent uniquement du PMSI car l'indication n'est pas renseignée
dans le DCIR (tables MEDAPAC + FH + FHSTC + indication `r params$code_indication`).

- IND_HAUT : une estimation haute avec l'indication d'intérêt
et les indications inconnues (non présentes dans le référentiel de l'ATIH). Les
données proviennent uniquement du PMSI car l'indication n'est pas renseignée
dans le DCIR (tables MEDAPAC + FH + FHSTC + indication `r params$code_indication` + indication(s) inconnue(s))

- IND_MAX : une estimation avec l'indication d'intérêt, les indications inconnues (données PMSI, idem IND_HAUT)
et les rétrocessions ex-OQN du DCIR (tables MEDAPAC + FH + FHSTC + indication `r params$code_indication` +
indication(s) inconnue(s) + ER_UCD_F toutes indications).

- TOT_PMSI : une estimation avec toutes les indications confondues en ne
retenant que les données du PMSI (tables MEDAPAC + FH + FHSTC, toutes indications). 

- TOT_DCIR : une estimation avec toutes les indications en comptabilisation les
dispensations de l'hospitalisation ex-DGF dans le PMSI mais en privilégiant la
source DCIR pour les hospitalisations ex-OQN et les rétrocessions ex-DGF ou
ex-OQN  (tables MEDAPAC + ER_UCD_F, toutes indications). 

A noter qu'un patient est inclus dans l'estimation basse (IND_BAS) ou haute
(IND_HAUT) dès lors qu'il a au moins une dispensation dans l'indication demandée
(et/ou dans une indication inconnue pour IND_HAUT), même s'il a également des
dispensations du médicament dans une autre indication (connue) que celle
demandée.

```{r nbpatients}
kable(
  summary_sources |>
    filter(source %in% c(
      constants$LABEL_SHORT_EST_TOT_DCIR,
      constants$LABEL_SHORT_EST_TOT_PMSI,
      constants$LABEL_SHORT_EST_IND_BAS,
      constants$LABEL_SHORT_EST_IND_HAUT,
      constants$LABEL_SHORT_EST_IND_MAX
    )),
  caption = "Nombre de patients distincts et de dispensations par source de données.",
  digits = 1,
  align = "l",
  table.attr = "style='width:100%;'"
)
```
