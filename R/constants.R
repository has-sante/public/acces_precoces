#' Produit les variables de constantes (ex. chemins vers les fichiers de
#' résultats).
#'
#' @export
Constants_rapac <- function() {
  constants <- list()
  constants$IS_PORTAIL <- dir.exists("~/sasdata1")
  if (!constants$IS_PORTAIL) {
    root <- rprojroot::is_r_package
    PAT2PKG_DATA <- system.file("extdata", package = "rapac")
    constants$PATH2DATA <- root$find_file(file.path("inst", "extdata"))
  } else {
    constants$PATH2DATA <- here::here(file.path("inst", "extdata"))
    PAT2PKG_DATA <- constants$PATH2DATA
  }

  constants$PATH2TMP <-
    file.path(constants$PATH2DATA, "intermediaires")
  constants$FILENAME_CONSOS <- "consos.parquet"
  constants$FILENAME_EVENTS <- "events.parquet"
  constants$FILENAME_PATIENT_FEATURES <- "patient_features.parquet"
  constants$FILENAME_AGG_RESULTS <- "aggregated_results.xlsx"
  constants$PATH2RESULTS <-
    file.path(constants$PATH2DATA, "resultats")

  # Global variables du paquet

  constants$PATH2REFS <-
    file.path(PAT2PKG_DATA, "referentiels")
  constants$PATH2EXAMPLES <-
    file.path(PAT2PKG_DATA, "examples")


  constants$DCIR_JOIN_VARS <- c(
    "FLX_DIS_DTD",
    "FLX_TRT_DTD",
    "FLX_EMT_TYP",
    "FLX_EMT_NUM",
    "FLX_EMT_ORD",
    "ORG_CLE_NUM",
    "DCT_ORD_NUM",
    "PRS_ORD_NUM",
    "REM_TYP_AFF"
  )
  constants$COLNAMES_PMSI <- c(
    "BEN_NIR_PSA",
    "UCD_UCD_COD",
    "ADM_NBR",
    "EXE_SOI_DTD",
    "EXE_SOI_DTF",
    "datetime_administration",
    "ETA_NUM_GEO",
    "STA_ETA",
    "year",
    "COD_LES"
  )
  constants$COLNAMES_UCD_CONSOS <- c(
    "BEN_IDT_ANO",
    "date_administration",
    "ucd_7",
    "indication",
    "nb_administrations",
    "source",
    "circuit",
    "finess_geo_executant",
    "STA_ETA"
  )
  constants$COL_START <- "start"
  constants$COL_END <- "end"
  constants$COL_SOURCE <- "source"
  constants$COL_TYPE <- "type"
  constants$COL_CODE <- "code"
  constants$COL_LABEL <- "label"
  constants$COLNAMES_EVENTS <- c(
    "BEN_IDT_ANO",
    constants$COL_START,
    constants$COL_END,
    constants$COL_TYPE,
    constants$COL_SOURCE,
    constants$COL_CODE,
    constants$COL_LABEL
  )
  constants$ACT_CODES_AP <- c("PHX", "PHH", "PHU", "PHY")
  constants$PRS_NAT_REF_CODES_AP <- c("3336", "3317", "3351", "3421")


  constants$TOP_PATHOS_TOP_LVL <- list(
    CAN_CAT_CAT = "Cancers",
    MCV_CAT_CAT = "Maladies cardioneurovasculaires",
    TFC_CAT_INC = "Traitements du risque vasculaire",
    # \n(avec ou sans pathologies)",
    DIA_CAT_CAT = "Diabète",
    PST_CAT_CAT = "Maladies psychiatriques",
    TPS_CAT_INC = "Traitements psychotropes",
    # \n(avec ou sans pathologies)",
    NEU_CAT_CAT = "Maladies neurologiques ou dégénératives",
    RES_CAT_INC = "Maladies respiratoires chroniques",
    # \n(avec ou sans pathologies)",
    IRV_CAT_CAT = "Maladies inflammatoires ou rares ou VIH ou sida",
    IRT_CAT_CAT = "Insuffisance rénale chronique terminale",
    MFP_CAT_INC = "Maladie du foie ou du pancréas",
    # avec ou sans mucoviscidose"
    ALD_CAT_CAT = "Autres ALD",
    MAT_CAT_INC = "Maternité",
    # avec ou sans pathologie
    INF_COV_HOS = "Séjour pour COVID-19",
    HHP_CAT_INC = "Hospitalisation hors pathologies repérées",
    # avec ou sans pathologie, traitement, maternité
    POP_PMH_ABS = "Pas de pathologies, traitements, maternité ou hospitalisations"
  )
  constants$TOP_PATHOS_CANCER <- list(
    CAN_SEI_CAT = "Cancer du sein",
    CAN_SEI_ACT = "Cancer du sein actif",
    CAN_SEI_SUR = "Cancer du sein sous surveillance",
    CAN_CRE_CAT = "Cancer colorectal",
    CAN_CRE_ACT = "Cancer colorectal actif",
    CAN_CRE_SUR = "Cancer colorectal sous surveillance",
    CAN_BPU_CAT = "Cancer du poumon",
    CAN_BPU_ACT = "Cancer du poumon actif",
    CAN_BPU_SUR = "Cancer du poumon sous surveillance",
    CAN_PRO_CAT = "Cancer de la prostate",
    CAN_PRO_ACT = "Cancer de la prostate actif",
    CAN_PRO_SUR = "Cancer de la prostate sous surveillance",
    CAN_AUT_CAT = "Autres cancers",
    CAN_AUT_ACT = "Autres cancers actifs",
    CAN_AUT_SUR = "Autres cancers sous surveillance"
  )
  # Sources
  constants$S_MEDAPAC <- "MEDATU/MEDAPAC, toutes indications"
  constants$S_FH <- "FH"
  constants$S_FHSTC <- "FHSTC"
  constants$S_ER_UCD_F <- "ER_UCD_F"
  # Circuits
  constants$C_HOSP <- "Hospitalisation"
  constants$C_RETRO <- "Rétrocession"
  # Labels pour le rapport
  constants$LABEL_INIT <- "Nb initiations"
  constants$LABEL_DISPENSATIONS <- "Nb de dispensations"
  constants$LABEL_CAT_ETE <- "Catégorie d'établissement"
  constants$LABEL_NB_PATIENTS <- "Nombre de patients"

  constants$LABEL_SHORT_EST_TOT_DCIR <- "TOT_DCIR"
  constants$LABEL_SHORT_EST_TOT_PMSI <- "TOT_PMSI"
  constants$LABEL_SHORT_EST_IND_BAS <- "IND_BAS"
  constants$LABEL_SHORT_EST_IND_MAX <- "IND_MAX"
  constants$LABEL_SHORT_EST_IND_HAUT <- "IND_HAUT"

  # excel sheet names
  constants$WS_TYPE <- "resume_type"
  constants$WS_ETE_CAT <- "resume_categorie_etablissement"
  constants$WS_CONSOS_MOIS <- "consos_par_mois"
  constants$WS_AGE_SEXE <- "patients_age_sexe"
  constants$WS_ALD <- "ald"
  constants$WS_TOP_PATHOS <- "top_pathologies"
  constants$WS_SOURCES <- "sources"

  return(constants)
}

#' Retourne le fichier de paramétrage pour étudier plusieurs médicaments.
#'
#' Contient les informations de période d'études associées à des noms de
#' médicament et d'indication.
#'
#' @return Data frame
#'
#' @export
get_has_parameters <- function() {
  constants <- Constants_rapac()
  parametres_has <-
    read.csv(file.path(constants$PATH2REFS, "parametres_has.csv"))
  return(parametres_has)
}

#' Retourne le référentiel ATIH en data frame à partir du nom du
#' fichier paramétré par l'utilisateur.
#'
#' @param reference_filename Nom du fichier contenant le référentiel
#' d'indications. Ce fichier doit être téléchargé par l'utilisateur sur le [site
#' de
#' l'ATIH](https://www.atih.sante.fr/medicament-en-aap-aac-et-cpc-ex-atu-et-post-atu)
#' puis placé dans le dossier `inst/extdata/referentiels/`.
#'
#' @return Data frame. Référentiel d'indications de médicaments en accès
#' précoces.
#'
#' @export
get_indications <- function(reference_filename) {
  constants <- Constants_rapac()
  indications <- read.csv(
    file.path(constants$PATH2REFS, reference_filename),
    sep = ";",
    encoding = "latin1"
  )
  return(indications)
}
