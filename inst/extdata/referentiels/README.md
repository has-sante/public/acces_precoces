# Référentiels indication des médicaments en accès précoces

Les indications des accès précoces sont disponibles soit dans: 

- `lst_ref_fichcomp_aap_aac_medatu_082024.csv` : le référentiel des indications de l'ATIH (le fichier d'août 2024 contient 2932 lignes) [téléchargé sur leur site](https://www.atih.sante.fr/medicament-en-aap-aac-et-cpc-ex-atu-et-post-atu).

- le tableau `tableau_codage_par_indication_ap_novembre_2023_publication.xlsx` du ministère de la santé, tableau "produits basculés en AAP" (version de novembre 2023). L'onglet *AP en cours est terminée* de ce tableau contient uniquement 278 lignes:  https://sante.gouv.fr/soins-et-maladies/medicaments/professionnels-de-sante/autorisation-de-mise-sur-le-marche/article/autorisation-d-acces-precoce-autorisation-d-acces-compassionnel-et-cadre-de

- Le fichier `cnam_liste_ucd_AP_les_retro.xlsx` correspond au référentiel utilisé par la CNAM pour le rapport charges et produits 2024. Il correspond au fichier disponible sur le site de ministère dont certaines lignes ont été retouchées (notamment les dates administratives des médicaments en rétrocession). Il continent non seulement les médicaments en accès précoces mais également les médicaments en ex-ATU. 

Nous n'utilisons que le référentiel de l'ATIH qui semble plus complet et sert de référence pour les données du SNDS.
