---
title: "Etude de faisabilité"
output: 
  pdf_document: default
author: "Matthieu Doutreligne (m.doutreligne@has-sante.fr)"
date: "`r Sys.Date()`"
params:
  nom_referentiel_indications: "lst_ref_fichcomp_aap_aac_medatu_082024.csv"
  force_computation: FALSE
editor_options: 
  markdown: 
    wrap: 72
---

# Objectif du script

Ce script permet de répliquer l'étude de faisabilité de l'estimation des
consommations de médicaments en accès précoces à partir du SNDS. Il
décrit des estimatiosn pour des médicaments en accès précoces selon
différents circuits de dispensation: liste en sus publique ou privée,
rétrocessions. Pour chacun, il compare les estimations à partir du SNDS
à celles déclarées par les industriels dans les Protocoles d'Utilisation
Thérapeutique et de Recueil de Données (PUT-RD).

# Guide d'utilisation

Ce script comporte deux étapes : - Extraction des données dans un
fichier csv `inst/extdata/resultats/faisabilite.csv` - Visualisation des
résultats et sauvegarde des deux visualisations dans le dossier
`inst/extdata/resultats`

La première étape doit être lancée sur le portail de l'assurance maladie
avec le paramètre `force_computation=TRUE`.

Il faut deux fichiers de paramètres pour fonctionner (déjà présents dans
le dossier `inst/extdata/referentiels/`):

-   `lst_ref_fichcomp_aap_aac_medatu_082024.csv` : Fichier ATIH
    contenant les codes et libellés indications des médicaments en accès
    précoces (version mai 2024)

-   `parametres_has.R` : Fichier de paramétrage de l'étude contenant
    pour les différents médicaments étudiées les estimations des PUT-RD
    et les dates de début et fin de période d'étude du PUT-RD.

```{r, echo=FALSE, message=FALSE, warning=FALSE}
# knit: (
#         function(inputFile, encoding) {
#
#         output_file <- file.path("../data/resultats", "faisabilite.pdf")
#
#         rmarkdown::render(inputFile, encoding=encoding, output_file=output_file) })


knitr::opts_chunk$set(
  echo = FALSE, warning = FALSE, message = FALSE, eval = TRUE
)

library(here)
source(here("R/constants.R"))
source(here("R/extraction_utils.R"))
source(here("R/rapport_utils.R"))

library(openxlsx)
library(knitr)
library(ggplot2)
library(dplyr)
library(lubridate) # for date object
library(arrow) # write compressed tables
library(stringr)

constants <- Constants_rapac()
```

# Extraction des données depuis le SNDS (uniquement sur le portail)

```{r}
# Médicaments
drug_names <- c(
  "Trodelvy",
  "Dupixent",
  "Enhertu",
  "Keytruda",
  "Ayvakyt",
  "Crysvita",
  "Imcivree",
  "Scemblix",
  "Xenpozyme"
)


if (params$force_computation) {
  library(ROracle) # charger le package Roracle
  drv <- dbDriver("Oracle") # obtenir le pilote pour se connecter à une base oracle
  conn <- dbConnect(drv, dbname = "IPIAMPR2.WORLD") # se connecter à la base IPIAMPR2.WORLD
  Sys.setenv(TZ = "Europe/Paris") # fuseaux horaires
  Sys.setenv(ORA_SDTZ = "Europe/Paris")

  all_results <- list()

  for (drug_name in drug_names) {
    print(drug_name)
    parametres_has <- get_has_parameters()
    code_indications <- parametres_has %>%
      filter(Médicament == drug_name) %>%
      select("Code.Indication") %>%
      pull()

    for (code_indication in code_indications) {
      print(code_indication)

      indication_params <- parametres_has %>%
        filter((Code.Indication == code_indication) & (Médicament == drug_name))
      indication_short <- indication_params %>%
        select("Indication") %>%
        pull()

      start_date <- indication_params %>%
        select("début.de.période") %>%
        pull()

      end_date <- indication_params %>%
        select("fin.de.période") %>%
        pull()

      path2study <- get_path2study(drug_name, code_indication, start_date, end_date)
      if (!file.exists(path2study)) {
        extraction_data <- extract_acces_precoces(
          conn,
          drug_name,
          code_indication,
          start_date,
          end_date
        )

        create_rapport_data(
          drug_name,
          start_date,
          end_date,
          reference_filename = nom_referentiel_indications,
          code_indication = code_indication,
          private = FALSE,
          indications_multiples = indication_params$indications_multiples
        )
      }
      workbook <- loadWorkbook(file.path(path2study, constants$FILENAME_AGG_RESULTS))
      summary_sources <- read.xlsx(workbook, sheet = constants$WS_SOURCES)

      drug_result <- list(
        drug_name = drug_name,
        code_indication = code_indication,
        indication_short = indication_short,
        indications_multiples = indication_params$indications_multiples,
        liste_en_sus = indication_params %>% pull(liste_en_sus),
        retrocession = indication_params %>% pull(retrocession),
        start_date = start_date,
        end_date = end_date,
        putrd_haut = indication_params %>% pull(n_inclus),
        putrd_bas = indication_params %>% pull(n_fiche_traitement),
        ind_bas = summary_sources %>% filter(source == constants$LABEL_EST_BASSE) %>% pull(n_patients),
        ind_haut = summary_sources %>% filter(source == constants$LABEL_EST_HAUTE) %>% pull(n_patients),
        tot_pmsi = summary_sources %>% filter(source == constants$LABEL_EST_TOTAL_SNDS_PMSI) %>% pull(n_patients),
        tot_snds = summary_sources %>% filter(source == constants$LABEL_EST_TOTAL_DCIR) %>% pull(n_patients),
        ind_max = summary_sources %>% filter(source == constants$LABEL_EST_IND_MAX) %>% pull(n_patients)
      )
      all_results[[paste0(drug_name, "_", code_indication)]] <- drug_result
    }
  }

  all_results_df <- bind_rows(all_results)
  write.csv(all_results_df, file.path(constants$PATH2RESULTS, "faisabilite.csv"), row.names = FALSE)
}
```

# Résultats

```{r}
library(ggplot2)
library(gridExtra)
library(dplyr)
constants <- Constants_rapac()

faisabilite <- read.csv(file.path(constants$PATH2RESULTS, "faisabilite.csv")) %>%
  mutate(drug_name = factor(drug_name, levels = drug_names)) %>%
  arrange(drug_name, code_indication)
```

## Valeurs absolues

Les colonnes d'estimation sont les suivantes :

-   putrd_haut : patients inclus dans le PUTRD
-   putrd_bas : patients avec une fiche de traitement reçue par
    l'industriel dans le PUTRD
-   ind_bas : estimation avec le PMSI et l'indication stricte
-   ind_haut : estimation avec le PMSI et l'indication non stricte
    (indication stricte + toutes les indications inconnues)

Nous avons également calculé trois autres estimations mais ne les avons
pas restituées sur ces graphes focalisés sur des indications
spécifiques: 

- tot_pmsi : estimation avec les données PMSI toutes
indications 
- tot_dcir : estimation avec les données du DCIR (dans la
table ER_UDC_F)\
- ind_max : estimation (ind) + nombre de patients traités en accès
précoce toutes indications confondues en rétrocession d'un établissement
ex-OQN.

Les détails des calculs sont indiqués dans le code de
`rapac::create_rapport_data()`.

```{r, fig.show='hide'}
faisabilite_df <- faisabilite %>%
  mutate(labels = drug_name)

# Absolute value plot
faisabilite_retros <- faisabilite_df %>% filter(retrocession == "true")
g_retros <- plot_putrd(
  faisabilite_retros,
  scale = 80,
  breaks_by = 50,
  show_legend = TRUE
)
faisabilite_not_retro <- faisabilite_df %>% filter(retrocession != "true")
g_not_retros <- plot_putrd(
  faisabilite_not_retro,
  scale = 1000,
  show_drug_types = TRUE
)
full_plot <- grid.arrange(g_retros, g_not_retros, nrow = 2, heights = c(2, 3))

# does not success in having a permanent path in rmarkdown
img_name <- "synthese_faisabilite_absolute"
path2img <- file.path(constants$PATH2RESULTS, paste0(img_name, ".pdf"))
# if packaged rprojroot::is_r_package$find_file("vignettes/img")
path2img_png <- file.path(constants$PATH2RESULTS, paste0(img_name, ".png"))
ggsave(path2img, plot = full_plot, units = "cm", width = 40, height = 40)
ggsave(path2img_png, plot = full_plot, units = "cm", width = 40, height = 40)
```

```{r}
include_graphics(path2img_png)
# ![Synthèse des résultats en valeurs absolues](inst/extdata/resultats/synthese_faisabilite_absolute.png)
```

## Valeurs relatives

```{r, width=40, height=40}
relative_plot <- plot_putrd(
  faisabilite_df,
  scale = 1,
  relative_nb_patients = TRUE,
  breaks_by = 0.5
)

img_name <- "synthese_faisabilite_relative"
path2img <- file.path(constants$PATH2RESULTS, paste0(img_name, ".pdf"))
# path2img_png <- file.path(rprojroot::is_r_package$find_file("vignettes/img"), paste0(img_name, ".png"))
path2img_png <- file.path(constants$PATH2RESULTS, paste0(img_name, ".png"))
ggsave(path2img, plot = relative_plot, units = "cm", width = 40, height = 40)
ggsave(path2img_png, plot = relative_plot, units = "cm", width = 40, height = 40)
```

```{r}
include_graphics(path2img_png)
# ![Synthèse des résultats en valeurs relatives](img/synthese_faisabilite_relative.png)#if vignette
```

## Tableau complet des résultats

-   Ecart SNDS à putrd_haut, % : Ecart minimal pour chaque médicament
    entre putrd_haut et ind_haut ou ind_bas, normalisé par putrd_haut :
    `min(ind_haut - putrd_haut, ind_bas - putrd_haut)/putrd_haut`

```{r}
library(kableExtra)
# pas de chevauchement entre la fourchette snds et la fourchette putrd

faisabilite_analyse <- faisabilite_df %>%
  select(-labels) %>%
  mutate(
    !!sym("ind_bas<= putrd_haut<= ind_haut") := ifelse(
      ((ind_bas <= putrd_haut) & (putrd_haut <= ind_haut)),
      "Oui",
      "Non"
    ),
    retrocession = ifelse(retrocession, "O", "N"),
    liste_en_sus = ifelse(liste_en_sus, "O", "N"),
    indications_multiples = ifelse(indications_multiples, "O", "N"),
    ecart_haut = putrd_haut - ind_haut,
    ecart_bas = putrd_haut - ind_bas
  ) %>%
  mutate(
    !!sym("Ecart relatif putrd_haut et ind_bas, %") := 100 * round(ecart_bas / putrd_haut, 3),
    !!sym("Ecart relatif putrd_haut et ind_haut, %") := 100 * round(ecart_haut / putrd_haut, 3)
  ) %>%
  select(-c(code_indication, start_date, end_date, ecart_bas, ecart_haut)) %>%
  rename("Médicament" = drug_name, "Indication" = indication_short, "Rétro- cession" = retrocession)

small_width <- "2em"

colnames(faisabilite_analyse) <- gsub("_", " ", colnames(faisabilite_analyse))

output_pdf <- TRUE

format <- ifelse(output_pdf, "latex", "html")
kable(
  faisabilite_analyse,
  format,
  caption = "Estimations du nombre de patients sur les périodes d'étude des PUTRD",
  digits = 1,
  align = "c",
  table.attr = "style='width:100%;'",
  # width = col_widths,
  booktabs = TRUE
) %>%
  kable_styling(latex_options = c("scale_down")) %>%
  column_spec(2, width = "6em") %>%
  column_spec(3, width = "4em") %>%
  column_spec(4, width = "3em") %>%
  column_spec(5, width = "4em") %>%
  column_spec(6, width = small_width) %>%
  column_spec(7, width = small_width) %>%
  column_spec(8, width = small_width) %>%
  column_spec(9, width = small_width) %>%
  column_spec(10, width = small_width) %>%
  column_spec(11, width = "3em") %>%
  column_spec(12, width = "5em") %>%
  column_spec(13, width = "4em")
```
