# Exploitation des données du SNDS pour contextualiser l’utilisation des médicaments en accès précoces

## Description

La HAS a souhaité investiguer les possibilités de recours au SNDS pour obtenir des
données descriptives sur les patients traités par un médicament délivré dans le
cadre d'une autorisation d'[accès
précoces](https://www.has-sante.fr/jcms/r_1500918/fr/acces-precoce-a-un-medicament)
(AAP). L'objectif est à terme de générer ces données de
contextualisation en remplacement
des données collectées dans le cadre des protocoles d'utilisation thérapeutique
et de recueil des données (PUT-RD).

Les livrables de ce travail sont : 

- un rapport de faisabilité selon les différents circuits de dispensation des médicaments, 
- un outil de requêtage permettant de produire de façon automatique un rapport d'analyse descriptive des données.

Ce projet a démarré en octobre 2023. La publication de cette documentation a été réalisée en octobre 2024.

Pour mener à bien ce projet, la mission data a développé un programme R, dont l'objectif principal est de produire un rapport automatisé sur l'utilisation d'un médicament en accès précoce à partir des données du SNDS. 


## Contexte technique

### Données 

Les données sont issues du SNDS (Système National des Données de Santé) et sont hébergées sur le [portail de l'assurance maladie (CNAM)](https://portail.sniiram.ameli.fr/). Pour mener cette étude, il est nécessaire d'avoir un accès aux [données individuelles bénéficiaires exhaustives de consommation de soins](https://documentation-snds.health-data-hub.fr/snds/formation_snds/documents_cnam/guides_pedagogiques_snds/guide_pedagogique_acces_permanents.html#qui-a-acces-au-snds-et-a-quelles-donnees). La HAS dispose d'un accès permanent aux données du SNDS (profil 108). 

Les tables contenant les dispensations de médicaments en accès précoce sont MEDATU / MEDAPAC, FH, FHSTC pour le PMSI (MCO, SSR et HAD), ainsi que la table ER_UCD_F pour le DCIR. Pour plus d'information, se référer au [rapport de faisabilité](https://www.has-sante.fr/jcms/p_3552759/fr/rapport-de-faisabilite-snds-acces-precoces).

### Schéma de flux de données simplifié

![](man/figures/acces_precoces_flux_simple.png)

### Technologies

- Langage de programmation : R
- Packages utilisés : haven, here, dplyr, dbplyr, openxlsx, lubridate, arrow

### Besoin de maintenance

Fichiers de références à mettre à jour : 

- **Dans les paramètres (utilisateur):** Fichier référentiel des médicaments en accès précoce de l'ATIH. A mettre à jour mensuellement par l'utilisateur tel que décrit sur la page de [prise en main](articles/rapac.html#Paramètrage). 

- **Dans le code (développeur)** : Version de la cartographie des pathologies de la CNAM : A mettre à jour annuellement au cours du second semestre de l'année, tel que décrit dans la vignette d'[installation et de maintenance du programme](articles/developpement.html#maintenance-du-programme). La version utilisée ici est celle de 2022 (CRTO_CT_IND_G11_2022).

## Liens utiles 

- [Lien vers cette page de la documentation](https://has-sante.pages.has-sante.fr/public/acces_precoces/)

- [Code source](https://gitlab.has-sante.fr/has-sante/public/acces_precoces)

- [Description des données](articles/data.html)

- [Prise en main](articles/rapac.html)

- [Exemple de rapport automatisé](articles/rapport.html)

- [Documentation des fonctions utilisées](reference/index.html)

- [Méthodologie SNDS](articles/methodologie_snds.html)

- [Rapport de faisabilité](https://www.has-sante.fr/jcms/p_3552759/fr/rapport-de-faisabilite-snds-acces-precoces) 

## Contacts 

- Mission data : data@has-sante.fr
- Cellule de Coordination des Données en Vie Réelle : contact.cellule.vie.reelle@has-sante.fr 
