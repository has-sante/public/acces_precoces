---
title: "Méthodologie SNDS"
output: 
  html_document:
    base_format: rmarkdown::html_vignette
  pdf_document:
    extra_dependencies: ["float"]
subtitle: "Haute Autorité de Santé"
date: "Octobre 2024"
author: 
  - Matthieu Doutreligne (ingénieur de données, Mission data)
  - Catherine Bisquay (ingénieure de données experte SNDS, Mission data)
  - Pierre-Alain Jachiet (responsable de la Mission data)
vignette: >
  %\VignetteIndexEntry{Méthodologie SNDS}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
resources: 
  - name: img
    files: 
      - img/
---


# 1 - Présentation du projet

Afin de simplifier et d'optimiser les collectes de données dans le cadre des
protocoles d'utilisation thérapeutique et de recueil des données (PUT-RD), la
HAS a investigué les possibilités de recours au SNDS pour obtenir des données
descriptives sur les patients traités par un médicament délivré dans le cadre
d'une autorisation d'[accès
précoces](https://www.has-sante.fr/jcms/r_1500918/fr/acces-precoce-a-un-medicament)
(AAP).  Pour mener à bien ce projet, la Mission data de la HAS, en lien avec la
Cellule de coordination des données en vie réelle, a développé un outil de
requêtage à partir d'un programme R permettant de produire de façon automatique
un rapport d'analyse descriptive sur l'utilisation d'un médicament en accès
précoce à partir des données du SNDS.  Cet outil nécessite que l'utilisateur
renseigne dans le programme les paramètres sur le médicament en accès précoce
qu'il veut étudier, tels que son code UCD, son indication ou les dates de
dispensation qui l'intéressent.  Ce document décrit uniquement la méthodologie
SNDS appliquée pour l'extraction des données et la création du rapport d'analyse
descriptive. Pour plus de précisions sur l'utilisation du programme R, vous
pouvez vous référez à [la documentation technique en ligne](
https://has-sante.pages.has-sante.fr/deai/acces_precoces/articles/rapac.html).

# 2 - Sélection de la population

## Critères d'inclusion

On inclut les patients avec au moins une dispensation  du médicament :

-	dans le cadre d'un accès précoce : le médicament demandé par l'utilisateur (à
partir du code UCD ou du nom du médicament) doit être inclus dans la liste des
médicaments en accès précoce  et délivré dans le cadre d'une AAP . Ce dernier
critère permet de ne pas prendre en compte les médicaments qui seraient
également inscrits sur la liste en sus ou pris en charge dans le droit commun
dans une autre indication.

-	durant la période d'étude demandée ; cette sélection est effectuée à partir de
la date de dispensation (cf. partie « Extraction des données de dispensations
des médicaments » pour le calcul de cette variable). A noter que la période
d'étude est celle définie par l'utilisateur, qu'elle soit ou non comprise dans
la période de validité administrative du médicament étudié.

## Filtres qualité

Dans le PMSI, les dispensations avec au moins un code retour erroné sur
l'identifiant patient sont exclues.

Dans le DCIR, les filtres qualité suivants sont appliqués : 

-	Sélection uniquement des patients avec des NIR certifiés ou provisoires
(variable `BEN_CDI_NIR` égale à "00", "03" ou "04") ;

-	Exclusion de l'activité des établissements de santé ex-DGF (données transmises
pour information et données des établissement en facturation directe).

# 3 - Extraction des données individuelles depuis le SNDS

Le programme extrait deux types d'information depuis le SNDS : 

-	Les dispensations de médicaments en accès précoce ; 

-	Les caractéristiques des patients correspondants. 

## Extraction des données de dispensations de médicaments en accès précoce 

Les dispensations de médicaments en accès précoces peuvent être identifiées dans
plusieurs tables du SNDS comme indiqué dans le Tableau 1 ci-dessous.  A noter
qu'un établissement ex-DGF correspond à un établissement public ou à un
établissement privé participant au service public hospitalier, et qu'un
établissement ex-OQN correspond à un établissement à but lucratif ou non
lucratif mais ne participant pas au service public hospitalier.

Tableau 1 : Résumé des différentes tables permettant d'identifier les
dispensations de médicaments en accès précoces dans le SNDS

| Sources de données                                                                                | PMSI (champ MCO, SSR ou HAD)      | PMSI (champ MCO, SSR ou HAD) | PMSI (champ MCO ou SSR) | DCIR     |
| ------------------------------------------------------------------------------------------------- | --------------------------------- | ---------------------------- | ----------------------- | -------- |
| Tables                                                                                            | MEDATU (<2022) / MEDAPAC (>=2022) | FH                           | FHSTC                   | ER_UCD_F |
| Médicament délivré au cours d'une hospitalisation dans un établissement ex-DGF                    | X                                 |                              |                         |          |
| Médicament délivré au cours d'une hospitalisation dans un établissement ex-OQN                    |                                   | X                            |                         | X        |
| Médicament rétrocédé en PUI d'un établissement ex-DGF                                             |                                   |                              | X                       | X        |
| Médicament rétrocédé en PUI d'un établissement ex-OQN                                             |                                   |                              |                         | X        |
| Code de l'indication                                                                              | X                                 | X                            | X                       |          |

### PMSI  
  
Le PMSI contient les données de dispensation des médicaments en accès précoce
pour les hospitalisations ex-DGF et ex-OQN et pour les rétrocessions en
établissement ex-DGF. 

A noter que :

-	Les données du PMSI ne contiennent pas les médicaments en accès précoces
dispensés dans la cadre d'une rétrocession en établissement ex-OQN.

-	Pour le SSR, les codes des indications (`COD_LES`) ne sont disponibles qu'à
partir de 2024.

-	Pour les hospitalisations ex-OQN et les rétrocessions en établissement ex-DGF,
deux façons de calculer la date de dispensation du médicament sont possibles.
Nous avons choisi d'utiliser la date de dispensation directement disponible dans
les tables « FH », plutôt que de la recalculer à partir de la date d'entrée et
du délai de dispensation depuis cette date.

-	La catégorie de l'établissement présentée dans le rapport provient du
référentiel Datasanté (table finess, `CATEG_LIB`) disponible dans le répertoire
rfcommun du portail.  Les dispensations de médicament en accès précoce sont
extraites depuis le PMSI (MCO, SSR, HAD) selon les modalités suivantes : 

#### Pour les **hospitalisations en établissement ex-DGF**

- Les dispensations (`UCD_UCD_COD` pour le code UCD du médicament, `COD_LES` pour
l'indication, `ADM_NBR` pour la quantité administrée) sont extraites depuis les tables MEDATU avant 2022 et MEDAPAC à
partir de 2022.

- La date de dispensation est calculée ainsi : date entrée du séjour
(`EXE_SOI_DTD`, table C) + délai de dispensation depuis la date d'entrée
(`DAT_DELAI`, tables MEDATU ou MEDAPAC). Dans le cas où la date de dispensation
calculée serait supérieure ou égale à la date de sortie du séjour
(`EXE_SOI_DTF`, table C), on lui affecte la valeur de la date d'entrée du
séjour.

- L'identifiant patient (`NIR_ANO_17`) provient de la table C.

- Le finess géographique (`ETA_NUM_GEO`) provient de la table UM pour le MCO
(finess de la première unité médicale du séjour hospitalier), de la table B
pour le SSR et l'HAD

- Le statut de l'établissement (`STA_ETA`) provient de la table E.

Le code source pour extraire ces données est disponible dans la fonction :
`extract_medapac_hospit_publique()`.

#### Pour les **hospitalisations en établissement ex-OQN**

- Les dispensations (`UCD_UCD_COD` pour le code UCD du médicament, `COD_LES` pour
l'indication, `QUA_COD` pour la quantité administrée et `EXE_SOI_DTD` pour la date de dispensation) sont extraites
depuis la table FH. Dans le cas où la date de dispensation serait supérieure
ou égale à la date de sortie du séjour (`EXE_SOI_DTF`, table C), on lui affecte
la valeur de la date d'entrée du séjour (`EXE_SOI_DTD`, table C).

- L'identifiant patient (`NIR_ANO_17`) provient de la table C.

- Le finess géographique (`ETA_NUM_GEO`) provient de la table UM pour le MCO
(finess de la première unité médicale du séjour hospitalier), de la table B
pour le SSR et la HAD.

- Le statut de l'établissement (`STA_ETA`) provient de la table E.

- Le code de l'acte (`ACT_COD`) permettant d'identifier les dispensations du
médicament dans le cadre d'un accès précoce provient des tables FB ou FC.
Cette variable permet de s'assurer que le médicament renseigné dans FH est
bien dispensé pour une AAP, dans le cas où ce médicament serait également
inscrit sur la liste en sus ou pris en charge dans le droit commun. Les codes
retenus pour la sélection des médicaments en accès précoce sont : PHX
(Pharmacie sous ATU séjour), PHH (Pharmacie hospitalière rétrocédée), PHU
(Médicament avec ATU nominative), PHY (Pharmacie en AAC et AAP séjour SSR).

Le code source pour extraire ces données est disponible dans la fonction :
`extract_liste_en_sus_hospit_privee()`.

#### Pour les **rétrocessions en établissement ex-DGF** (uniquement pour MCO et SSR)

- Les dispensations (`UCD_UCD_COD` pour le code UCD du médicament, `COD_LES` pour
l'indication, `QUA` (MCO) / `QUA_COD` (SSR) pour la quantité administrée et `EXE_SOI_DTD` pour la date dispensation) sont extraites depuis la
table FHSTC.

- L'identifiant patient (`NIR_ANO_17`) provient de la table FCSTC.

- Le finess géographique (`ETA_NUM_GEO`) provient de la table FCSTC.

- Le statut de l'établissement (`STA_ETA`) provient de la table E.

- Le code de l'acte (`ACT_COD`) permettant d'identifier les dispensations du
médicament dans le cadre d'un accès précoce provient des tables FCSTC ou FBSTC
(cf. paragraphe précédent sur les hospitalisations ex-OQN pour plus de détail
sur cette variable).

Le code source pour extraire ces données est disponible dans la fonction :
`extract_ap_en_retrocession_publique()`.

### DCIR

Le DCIR contient les données de dispensation des médicaments en accès précoce
pour les hospitalisations ex-OQN et pour les rétrocessions en établissement
ex-DGF et ex-OQN. Ainsi, les dispensations de médicaments pour l'hospitalisation
ex-OQN et la rétrocession en établissement ex-DGF sont extraites en doublon avec
le PMSI. 

A noter que :

-	L'indication du médicament n'est pas une information disponible dans le DCIR.

-	La catégorie de l'établissement (`CATEG_LIB`) présentée dans le rapport provient du référentiel
Datasanté (table finess) disponible dans le répertoire rfcommun du portail.  

Les données sont extraites par date de flux avec un suivi de 6 mois au-delà de la
date de fin de la période d'étude (ou avec le suivi maximum disponible si les 6
mois de recul ne sont pas encore atteints au moment de l'étude), afin de prendre
en compte les délais de remontée.  Les dispensations de médicaments en accès
précoce sont extraites depuis le DCIR selon les modalités suivantes :

-	Les dispensations (`UCD_UCD_COD` pour le code UCD du médicament, `UCD_DLV_NBR` pour la quantité administrée et `EXE_SOI_DTD`
pour la date de dispensation) sont extraites depuis la table ER_UCD_F.

-	L'identifiant patient (`BEN_NIR_PSA` + `BEN_RNG_GEM`) provient de la table
IR_BEN_R.

-	Le finess géographique (`ETB_EXE_FIN`) provient de la table ER_ETE_F.

-	Le circuit de délivrance (rétrocession / hospitalisation) est issu de la
variable UCD_TOP_UCD de ER_UCD_F.

-	La nature de prestation (PRS_NAT_REF) provient de la table ER_PRS_F. Cette
variable est similaire à la variable `ACT_COD` du PMSI. Elle permet de s'assurer
que le médicament renseigné dans ER_UCD_F est bien dispensé pour une AAP, dans
le cas où ce médicament serait également inscrit sur la liste en sus ou pris en
charge dans le droit commun. Les codes retenus pour la sélection des médicaments
en accès précoce sont : 3336 (Pharmacie sous ATU séjour), 3317 (Pharmacie
hospitalière rétrocédée), 3351 (Médicament avec ATU nominative), 3421 (Pharmacie
en AAC et AAP séjour SSR).

Le code source pour extraire ces données est disponible dans la fonction :
`extract_ap_dcir()`.

### Gestion des données en doublon entre PMSI et DCIR

Les deux sources (PMSI et DCIR) sont pertinentes, selon le besoin de
l'utilisateur. Dans le rapport, nous restituons donc des estimations issues de
ces deux sources de données (cf. partie « Estimations du nombre de patients et
de dispensations lors de la production des données pour le rapport »).

## Extraction des caractéristiques des patients

Les caractéristiques des patients comprennent des données démographiques et des
données de comorbidités.

### Données démographiques  

La source de données est le référentiel `IR_BEN_R`. Les informations extraites
sont : 

-	L'âge à la première dispensation : calculé à partir du mois et de l'année de naissance en imputant systématiquement le jour de naissance au premier jour du mois ;

-	Le sexe.

Si plusieurs lignes existent pour un même patient (variable `BEN_IDT_ANO`)
concernant le mois de naissance, l'année de naissance ou le sexe, l'information
retenue correspond à celle de l'information la plus récente ( = dernière
prestation traitée pour le patient : tri de la table IR_BEN_R par ordre
décroissant de date de traitement d'une prestation (`MAX_TRT_DTD`), date de mise à
jour (`BEN_DTE_MAJ`) et date d'insertion (`BEN_DTE_INS`)). 

Le code source pour extraire ces données est disponible dans la fonction
`extract_patient_features()`.

### Données de comorbidité  

Les données de comorbidité proviennent de deux sources.

La première source de données est la **cartographie des pathologies** et des
dépenses de l'assurance maladie (table CRTO_CT_IND_G11_2022 , schéma MEPSGP_108,
répertoire ORAMEPS sous SAS). Les comorbidités retenues sont les niveaux les
plus hauts de la cartographie ainsi que le détail des cancers. 

Le code source pour extraire ces données est disponible dans la fonction :
`extract_patient_features()`.

La deuxième source de données concerne les **affections de longue durée** (ALD)
présentes dans la table IR_IMB_R. Pour un patient donné, seules les ALD déjà
en cours ou ayant débuté entre la première et dernière date des dispensations du
médicament incluses dans la période d'étude demandée sont retenues. Les motifs
d'exonération retenus pour sélectionner les ALD sont 41 ALD sur liste, 43 ALD
hors liste et 45 Polypathologie (variable `IMB_ETM_NAT`). Les diagnostics
présentés dans le rapport sont issus de la variable MED_MTF_COD associés au
libellé du référentiel CIM_10 (table IR_CIM_V). Seuls les diagnostics dont la
prévalence excède 1 % dans la population étudiée sont restitués visuellement. 

Le code source pour extraire ces données est disponible dans la fonction :
`extract_events()`.

## Jointure entre les données de caractéristiques des patients et les données de dispensations de médicaments  

Les extractions réalisées (sur les dispensations de médicaments et sur les
patients) sont jointes au référentiel des bénéficiaires afin d'attribuer un
BEN_IDT_ANO unique à chaque patient. 

Pour les tables issues du PMSI, cette jointure est faite sur la variable
`NIR_ANO_17` (équivalente à la variable `BEN_NIR_PSA` du DCIR) puis un _distinct_
est effectué par `BEN_IDT_ANO` afin de supprimer les doublons induits par la
jointure.

Pour les tables issues du DCIR, cette jointure est faite sur les variables
`BEN_NIR_PSA` et `BEN_RNG_GEM` (si la variable BEN_IDT_ANO n'est pas déjà présente
dans les extractions). 

# 4 - Estimations du nombre de patients et de dispensations lors de la production des données pour le rapport

Lors de la création du rapport, cinq estimations sont produites, et restituées
selon les choix de l'utilisateur :

- IND_BAS : une estimation basse avec l'indication d'intérêt demandée par
l'utilisateur. Les données proviennent uniquement du PMSI car l'indication n'est
pas renseignée dans le DCIR (tables MEDAPAC+FH+FHSTC + indication demandée).

- IND_HAUT : une estimation haute avec l'indication d'intérêt et les indications
inconnues (non présentes dans le référentiel de l'ATIH). Les données proviennent
uniquement du PMSI car l'indication n'est pas renseignée dans le DCIR (tables
MEDAPAC+FH+FHSTC + indication demandée + indication(s) inconnue(s)).

- IND_MAX : une estimation avec l'indication d'intérêt, les indications
inconnues (données PMSI, idem IND_HAUT) et les rétrocessions ex-OQN du DCIR
(tables MEDAPAC+FH+FHSTC + indication demandée + indication(s) inconnue(s)+
ER_UCD_F toutes indications).

- TOT_PMSI : une estimation avec toutes les indications confondues en ne
retenant que les données du PMSI (tables MEDAPAC+FH+FHSTC, toutes indications). 

- TOT_DCIR : une estimation avec toutes les indications en comptabilisation les
dispensations de l'hospitalisation ex-DGF dans le PMSI mais en privilégiant la
source DCIR pour les hospitalisations ex-OQN et les rétrocessions ex-DGF ou
ex-OQN (tables MEDAPAC+ER_UCD_F, toutes indications). 

A noter qu'un patient est inclus dans l'estimation basse (IND_BAS) ou haute
(IND_HAUT) dès lors qu'il a au moins une dispensation dans l'indication demandée
(et/ou dans une indication inconnue pour IND_HAUT), même s'il a également des
dispensations du médicament dans une autre indication (connue) que celle
demandée.

Pour bien comprendre la source utilisée pour ces différents calculs, nous avons
repris le Tableau 1 présenté plus haut pour chaque estimation dans la Figure 1.

![Figure 1 : Représentation des différents calculs du nombre de patients traités selon la source d'information](img/tableau_sources_donnees.png)

# 5 - Limites

L'analyse des différentes modalités de dispensations (rétrocessions /
hospitalisations, ex-DGF / ex-OQN) et des biais possiblement induits pour chaque
situation est détaillée dans la note de faisabilité publiée par la HAS. Les deux
principales limites identifiées sont les suivantes :

-	Pour les dispensations en rétrocession dans un établissement ex-OQN, il n'est
pas possible d'identifier l'indication car celle-ci n'est pas enregistrée dans
le DCIR (seule source de déclaration de ces dispensations). Si l'on s'intéresse
à une indication précise, une sous-estimation du nombre de dispensation est donc
possible, due à l'absence des médicaments en rétrocession des établissements
ex-OQN dans le PMSI.

-	Pour une indication donnée, des écarts importants peuvent exister entre les
estimations IND_HAUT et IND_BAS, probablement dus à des erreurs de codage sur la
variable indication (`COD_LES`) du PMSI.
