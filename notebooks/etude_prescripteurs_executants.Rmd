---
title: "extraction"
author: "Matthieu"
date: "`r Sys.Date()`"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

# Extraction des données accès précoces 

## Setups

```{r setup_oracle}
# knitr::opts_knit$set(root.dir = '../R/')

library(ROracle) # charger le package Roracle
drv <- dbDriver("Oracle") # obtenir le pilote pour se connecter à une base oracle
conn <- dbConnect(drv, dbname = "IPIAMPR2.WORLD") # se connecter à la base IPIAMPR2.WORLD
Sys.setenv(TZ = "Europe/Paris") # fuseaux horaires
Sys.setenv(ORA_SDTZ = "Europe/Paris")

library(here)
source(here("R/constants.R"))
source(here("R/codes_medicaments.R"))
source(here("R/extraction_utils.R"))
library(dplyr)
library(dbplyr)
library(lubridate) # for date objects
library(arrow) # write compressed tables
library(stringr)
```



## Extraction depuis le PMSI

Paramétrage de la requête: 

```{r }
start_date <- as.Date("2022-01-01")
end_date <- as.Date("2022-12-31")
drug_name <- "Trodelvy"
ucd_codes_list <- UCD_CODES[[drug_name]]

path2results_consos <- file.path(PATH2RESULTS, paste0(drug_name, "_", str_replace_all(start_date, "-", ""), "_", str_replace_all(end_date, "-", "")))
consos_finales <- read_feather(path2results_consos)
```



## Etude des prescripteurs

Quelle est la part des spécialités de prescripteurs renseignés, dans quel cas (rétrocession, hospitalisation privée) ? 

```{r}
n_consos_hors_hospitalisation_publique <- consos_finales %>%
  filter(!(source == S_HOSPI_PUB)) %>%
  count()

# Nombre de lignes avec une spécialisté de pfs exécutant (-> aucun n'est renseigné)
consos_w_spe_prescripteurs <- consos_finales %>%
  group_by(PSP_SPE_COD, source, PSE_STJ_COD) %>%
  count(sort = TRUE)

n_consos_w_spe_prescripteurs <- consos_finales %>%
  filter(
    !(source == S_HOSPI_PUB) &
      !(PSP_SPE_COD %in% c(99)) &
      !(is.na(PSP_SPE_COD))
  ) %>%
  count()

print(paste0("Part de finess prescripteurs remplis hors hospitalisation publique: ", n_consos_w_spe_prescripteurs, "/", n_consos_hors_hospitalisation_publique, "=", n_consos_w_spe_prescripteurs / n_consos_hors_hospitalisation_publique))
```


Quelle est la part des numéros finess des établissements prescripteurs est renseignée, dans quel cas (rétrocession, hospitalisation privée) ? 

```{r}
n_consos <- consos_finales %>% count()
consos_w_prescripteurs <- consos_finales %>%
  group_by(ETB_PRE_FIN, source, PSE_STJ_COD) %>%
  count(sort = TRUE)

n_consos_w_prescripteurs <- consos_w_prescripteurs %>%
  filter(!is.na(ETB_PRE_FIN)) %>%
  pull(n) %>%
  sum()

print(paste0("Part de finess prescripteurs remplis hors hospitalisation publique: ", n_consos_w_prescripteurs, "/", n_consos_hors_hospitalisation_publique, "=", n_consos_w_prescripteurs / n_consos_hors_hospitalisation_publique))
```


## Etablissments exécutants

Quelle est la part des numéros finess des établissements est renseignée hors hospitalisation publique, dans quel cas (rétrocession, hospitalisation privée) ? 

```{r}
consos_w_finess <- add_finess_info(consos_finales)

consos_w_finess_hors_public <- consos_w_finess %>% filter(!(source == S_HOSPI_PUB))
n_consos_w_finess_executant <- consos_w_finess_hors_public %>%
  filter(!is.na(finess_geo_executant)) %>%
  count()

print(paste0("Part de finess executant remplis hors hospitalisation publique: ", n_consos_w_finess_executant, "/", n_consos_hors_hospitalisation_publique, "=", n_consos_w_finess_executant / n_consos_hors_hospitalisation_publique))

consos_w_finess_hors_public %>%
  filter(is.na(rs)) %>%
  count()
```

Quelle est la part des numéros finess des établissements est renseignée en hospitalisation publique ? 

```{r}
consos_w_finess_en_public <- consos_w_finess %>% filter(source == S_HOSPI_PUB)
n_consos_en_hospitalisation_publique <- consos_w_finess_en_public %>% count()
n_consos_w_finess_executant_public <- consos_w_finess_en_public %>%
  filter(!is.na(finess_geo_executant)) %>%
  count()

print(paste0("Part de finess executant remplis hors hospitalisation publique: ", n_consos_w_finess_executant_public, "/", n_consos_en_hospitalisation_publique, "=", n_consos_w_finess_executant_public / n_consos_en_hospitalisation_publique))

# check du lien avec finess
consos_w_finess_en_public %>%
  filter(is.na(rs)) %>%
  count()
```
## Mini étude sur le format des finess dans dcir 

Tous les finess liés à une prescription dans er_ucd sont sur 8 caractères (que ce soit executant ou prescripteur). Donc on fera la jointure sur finess au format 8  avec la table datasante_t_finess.

```{r}
path2full_ap_from_er_ucd_f <- file.path(PATH2DATA_INTER, paste0("full_ap_from_er_ucd_f__", year))
ap_from_er_ucd_f_year <- read_feather(path2full_ap_from_er_ucd_f) %>%
  mutate(
    n_char_finess_prescripteur = nchar(ETB_PRE_FIN),
    n_char_finess_executant = nchar(ETB_EXE_FIN)
  )

ap_from_er_ucd_f_year %>%
  group_by(n_char_finess_prescripteur) %>%
  count()

ap_from_er_ucd_f_year %>%
  group_by(n_char_finess_executant) %>%
  count()
```


