/****************************************************************************************************************
 ACCES PRECOCE: 
 TEST SUR 2 MEDICAMENTS POUR VERIFIER LES EFFECTIFS ET VOIR QUELLES DONNEES DE CONTEXTUALISATION SERAIENT INTERESSANTES DANS LE SNDS

- SCEMBLIX (asciminib) 
- TRODELVY (sacituzumab govitecan)

DONNEES DU PMSI: 
	- FICHIERS FICHCOMP MEDATU AVANT 2022 et FICHCOMP MEDAPAC A PARTIR DE 2022: POUR HOSPITALISATION EN ETABLISSEMENT EX-DGF (PAS D'ETABLISSEMENT EX-OQN DANS CES FICHIERS)
	- FICHIER RSF-B OQN (FH): POUR HOSPITALISATION EN ETABLISSEMENT EX-OQN (DOUBLON AVEC ER_UCD_F DCIR) (exhaustivit�?)
	- FICHIER RSF-B ACE (FHSTC): POUR RETROCESSION ETABLISSEMENT EX-DGF (DOUBLON AVEC ER_UCD_F DCIR) (exhaustivit�?)
DONNEES DU DCIR:
	- TABLE ER_UCD_F: POUR HOSPITALISATION EN ETABLISSEMENT EX-OQN OU RETROCESSION ETABLISSEMENT EX-DGF ET EX-OQN
	- TABLE PHA: VERIF AU CAS OU => ON NE RETROUVE RIEN

ANNEES 2021 et 2022
	1 - EXTRACTION ET MISE EN FORME DONNEES PMSI MCO DGF HOSPIT 2021 et 2022
	2 - EXTRACTION ET MISE EN FORME DONNEES PMSI MCO DGF EXTERNE 2021 et 2022
	3 - EXTRACTION ET MISE EN FORME DONNEES PMSI MCO OQN HOSPIT 2021 et 2022
	4 - EXTRACTION ET MISE EN FORME DONNEES DCIR 2021 et 2022
	5 - MISE EN FORME DONNEES PMSI ET DCIR
	6 - RESTITUTION DES RESULTATS

=> Choix apr�s cette analyse: Utilisation des donn�es DCIR pour r�trocession et hospitalisation priv� et des donn�es PMSI pour hospitalisation publique (= tableau r�cap partie 5)

A FAIRE DANS UN SECOND TEMPS?:
- Si on veut suivre les patients (donn�es de contextualisation), faire une table avec tous leurs identifiants possibles dans IR_BEN_R
- Optimiser prog (macros par med / ann�e, recup automatique des libell�s UCD au lieu de faire des formats (si dispo dans les tables de ref SNDS???),...) 
- A faire sous R au lieu de SAS?

*****************************************************************************************************************/

/***************************************D�finition des macrovariables*********************************************************/
%let annee=2022;
%let annee1=2021;

/*PMSI*/
%let an=22;
%let table=medapac; /*2022: FICHCOMP AP (remplace FICHCOMP ATU)*/
%let an1=21;
%let table1=medatu; /*2021: FICHCOMP ATU*/

/*DCIR*/
%let date_debm=202101;
%let date_finm=202212;

/*M�dicaments (UCD ou CIP pour table PHA)*/
%let ucd7='9002285','9002286','9001995','9000921','9001664','0000009002285','0000009002286','0000009001995','0000009000921','0000009001664'; /*sur 7 caract�res ou sur 13 caract�res avec 0 devant*/
%let ucd13='3400890022859','3400890022866','3400890019958','3400890009218','3400890016643';
%let cip13=3400930258101,3400930258125,3400955085256;
%let cip7=3025810,3025812,5508525;
%let scemblix_ucd='3400890022859','9002285','3400890022866','9002286','0000009002285','0000009002286';
%let trodelvy_ucd='3400890019958','9001995','3400890009218','9000921','3400890016643','9001664','0000009001995','0000009000921','0000009001664';

/*date de traitement*/
data _null_;
call symput("datetrt",left(put("&sysdate"d,ddmmyy10.)));
run;
/*****************************************************************************************************************/

/***************************************D�finition des formats*********************************************************/
proc format;
picture commaxa (round)  low-<0 =  '00000000000009' (prefix='-' mult=1)
                         0-999999998 = '00000000000009' (mult=1) 
			 999999999 = 'NC'
			 .='NC';
picture commaxd (round)  low-<0 =  '000000000009.999' (prefix='-' mult=1000)
                         0-999999998 = '000000000009.999' (mult=1000)
			 999999999 = 'NC'
			 .='NC';

value $ucd
'3400890022859'='3400890022859 SCEMBLIX 20 MG'
'3400890022866'='3400890022866 SCEMBLIX 40 MG'	
'3400890019958'='3400890019958 TRODELVY 200 MG	'
'3400890009218'='3400890009218 SACITUZUMAB GOV IDS 180MG FL	'
'3400890016643'='3400890016643 SACITUZUMAB GOV GLD 180MG FL'
'9002285'='9002285 SCEMBLIX 20 MG'
'9002286'='9002286 SCEMBLIX 40 MG'
'9001995'='9001995 TRODELVY 200 MG'
'9000921'='9000921 SACITUZUMAB GOV IDS 180MG FL'
'9001664'='9001664 SACITUZUMAB GOV GLD 180MG FL'
'9999999'='TOTAL'
;

value topucd
0='R�trocession'
1='Hospitalisation ES priv�'
2='Hospitalisation ES public'
5="A0"X
;

value $indic_scemblix
'CASCI01'='CASCI01 Traitement des patients adultes atteints de leuc�mie my�lo�de chronique chromosome Philadelphie positive en phase chronique (LMC-PC Ph+) pr�c�demment trait�s par au moins deux inhibiteurs de tyrosine'
'NASCI02'='NASCI02 Leuc�mie aig�e lymphoblastique'
'NASCI03'='NASCI03 Leuc�mie my�lo�de chronique'
'NASCI04'="NASCI04 Monoth�rapie de la leuc�mie my�lo�de chronique (LMC) en phase chronique avec mutation T315I, pr�c�demment trait�e par tous les inhibiteurs de tyrosine kinase (ITK) disponibles sur le march� et ayant rechut�, ou r�fractaire ou intol�rance aux ITK d'apr�s l'�valuation du m�decin, ou pour qui le traitement par un ou plusieurs ITK disponibles est contre-indiqu�"
'NASCI07'="NASCI07 Monoth�rapie de la leuc�mie my�lo�de chronique (LMC) en phase chronique sans mutation T315I, pr�c�demment trait�e par tous les inhibiteurs de tyrosine kinase (ITK) disponibles sur le march� et ayant rechut�, ou r�fractaire ou intol�rance aux ITK d'apr�s l'�valuation du m�decin, ou pour qui le traitement par un ou plusieurs ITK disponibles est contre-indiqu�"
'NASCI05'='NASCI05 Leuc�mie my�lo�de chronique avec mutation T315i'
'NASCI06'='NASCI06 Leuc�mie my�lo�de chronique sans mutation T315i'
'NXXXX00'='NXXXX00 Leuc�mie my�lo�de chronique sans mutation T315i'
'NXXXX01'='NXXXX01 Autre'
;

value $indic_trodelvy
'CSACI01'="CSACI01 Traitement en monoth�rapie des patients adultes, ayant un cancer du sein triple n�gatif non r�s�cable ou m�tastatique, ayant re�u pr�alablement 2 lignes de traitement syst�miques ou plus, dont au moins l'une d'entre elles au stade avanc�"
'CSACI02'="CSACI02 En monoth�rapie pour le traitement des patients adultes atteints d'un cancer du sein RH positifs / HER2 n�gatifs (IHC 0, IHC 1+ ou IHC 2+/ISH-) non r�s�cable ou m�tastatique, ayant re�u au moins deux lignes de chimioth�rapie au stade m�tastatique"
'NSACI02'='NSACI02 Tumeur maligne du sein'
'NSACI03'="NSACI03 Monoth�rapie des patients adultes, ayant un cancer du sein triple n�gatif non r�s�cable ou m�tastatique, ayant re�u pr�alablement 2 lignes syst�miques de traitement ou plus, dont au moins l'une d'entre elles au stade avanc�"
'NXXXX00'="NXXXX00 Tumeur maligne du sein"
'NXXXX01'='NXXXX01 Autre'
;
run;

/*****************************************************************************************************************/

/**********************************************************1 - EXTRACTION ET MISE EN FORME DONNEES PMSI MCO DGF HOSPIT 2021 et 2022*******************************************/
/*
----------------------------------------------
2021: Table Fichcomp ATU
2022: Table Fichcomp AP/AC
----------------------------------------------
*/

/****************************Extraction*************************************/
proc sql;
create table selec_ap_pmsi_&annee as select distinct c.nir_ano_17 as ben_nir_psa,c.exe_soi_dtd,m.dat_delai,m.UCD_UCD_COD, m.cod_les,m.ADM_MOIS,m.ADM_ANN,sum(m.ADM_NBR) as adm_nbr, sum(m.SEJ_NBR) as sej_nbr
from oravue.t_mco&an.&table m
inner join oravue.t_mco&an.c c on (m.eta_num=c.eta_num and m.rsa_num=c.rsa_num)
inner join oravue.t_mco&an.b b on (m.eta_num=b.eta_num and m.rsa_num=b.rsa_num)
where c.NIR_RET='0' and c.NAI_RET='0' and c.SEX_RET='0' and c.SEJ_RET='0' and c.FHO_RET='0' and c.PMS_RET='0' and c.DAT_RET='0' and c.COH_NAI_RET='0' and c.COH_SEX_RET='0' and m.ucd_ucd_cod in (&ucd13,&ucd7) /*codage UCD 13 normalement*/
group by c.nir_ano_17,m.UCD_UCD_COD, m.cod_les,m.ADM_MOIS,m.ADM_ANN,c.exe_soi_dtd,c.exe_soi_dtf,m.dat_delai;
quit;

proc sql;
create table selec_ap_pmsi_&annee1 as select distinct c.nir_ano_17 as ben_nir_psa,c.exe_soi_dtd,m.dat_delai,m.UCD_UCD_COD, m.cod_les,m.ADM_MOIS,m.ADM_ANN,sum(m.ADM_NBR) as adm_nbr, sum(m.SEJ_NBR) as sej_nbr
from oravue.t_mco&an1.&table1 m
inner join oravue.t_mco&an1.c c on (m.eta_num=c.eta_num and m.rsa_num=c.rsa_num)
inner join oravue.t_mco&an1.b b on (m.eta_num=b.eta_num and m.rsa_num=b.rsa_num)
where c.NIR_RET='0' and c.NAI_RET='0' and c.SEX_RET='0' and c.SEJ_RET='0' and c.FHO_RET='0' and c.PMS_RET='0' and c.DAT_RET='0' and c.COH_NAI_RET='0' and c.COH_SEX_RET='0' and m.ucd_ucd_cod in (&ucd13,&ucd7) /*codage UCD 13 normalement*/
group by c.nir_ano_17,m.UCD_UCD_COD, m.cod_les,m.ADM_MOIS,m.ADM_ANN,c.exe_soi_dtd,c.exe_soi_dtf,m.dat_delai;
quit;

 /************Mise en forme des donn�es extraites et harmonisation avec les donn�es du DCIR**********************/
data selec_ap_pmsi;
set selec_ap_pmsi_&annee selec_ap_pmsi_&annee1;
format date_adm ddmmyy10.;
ucd7=substr(ucd_ucd_cod,6,7);
date_adm=datepart(exe_soi_dtd)+dat_delai;
nb_adm=adm_nbr/sej_nbr;
run;

/*Ajout du ben_idt_ano dans la table AP DCIR (suppression des patients non retrouv�s dans IR_BEN_R)*/
proc sql;
drop table orauser.extract_ap_ben;
create table orauser.extract_ap_ben as select distinct ben_nir_psa from selec_ap_pmsi;
quit;

proc sql;
create table extract_irbenr as select distinct b.ben_nir_psa, b.ben_rng_gem, b.ben_idt_ano 
from orauser.extract_ap_ben a 
inner join oravue.ir_ben_r b on (a.ben_nir_psa = b.ben_nir_psa);
quit;

proc sort data=extract_irbenr out=extract_irbenr_ nodupkey;by ben_nir_psa;run;

proc sql;
create table selec_ap_pmsi as select a.* , b.ben_idt_ano from selec_ap_pmsi a
inner join extract_irbenr_ b on (a.ben_nir_psa=b.ben_nir_psa);
quit; 

proc sql;
create table selec_ap_pmsi as select distinct ben_idt_ano,adm_ann,adm_mois,ucd7,cod_les as indication,sum(nb_adm) as nb_adm, count(distinct date_adm) as nb_del from selec_ap_pmsi
group by ben_idt_ano,adm_ann,adm_mois,ucd7,cod_les;
quit;

/************Mise en forme des r�sultats: TRODELVY*******************/
proc sql;
/*par ann�e*/
create table res_pmsi_trodelvy_1 as select distinct adm_ann,ucd7,count(distinct ben_idt_ano) as nb_patient,sum(nb_adm) as nb_adm,sum(nb_del) as nb_del from selec_ap_pmsi where adm_ann in ('2021','2022') and ucd7 in (&trodelvy_ucd) group by adm_ann,ucd7;
/*colonne Total*/
create table res_pmsi_trodelvy_2 as select distinct 'TOTAL' as adm_ann,ucd7,count(distinct ben_idt_ano) as nb_patient,sum(nb_adm) as nb_adm,sum(nb_del) as nb_del from selec_ap_pmsi where adm_ann in ('2021','2022') and ucd7 in (&trodelvy_ucd) group by ucd7;
/*lignes Total*/
create table res_pmsi_trodelvy_3 as select distinct adm_ann,'9999999' as ucd7,count(distinct ben_idt_ano) as nb_patient,sum(nb_adm) as nb_adm,sum(nb_del) as nb_del from selec_ap_pmsi where adm_ann in ('2021','2022') and ucd7 in (&trodelvy_ucd) group by adm_ann;
create table res_pmsi_trodelvy_4 as select distinct 'TOTAL' as adm_ann, '9999999' as ucd7,count(distinct ben_idt_ano)as nb_patient,sum(nb_adm) as nb_adm,sum(nb_del) as nb_del from selec_ap_pmsi where adm_ann in ('2021','2022') and ucd7 in (&trodelvy_ucd)  ;
quit;

data res_pmsi_trodelvy;
format adm_ann $5.;
set res_pmsi_trodelvy_1 res_pmsi_trodelvy_2 res_pmsi_trodelvy_3 res_pmsi_trodelvy_4 ;
run;

proc sort data=res_pmsi_trodelvy;by ucd7 adm_ann;run;

 /*Pour l'affichage de la sortie*/
 data out_pmsi_trodelvy;
 set res_pmsi_trodelvy;
 if nb_patient<11 then do;
 nb_patient=999999999;
 nb_adm=999999999;
 nb_del=999999999;
 end;
 run;

/*******************Mise en forme des r�sultats: SCEMBLIX****************/
proc sql;
/*par ann�e*/
create table res_pmsi_scemblix_1 as select distinct adm_ann,ucd7,count(distinct ben_idt_ano) as nb_patient,sum(nb_adm) as nb_adm,sum(nb_del) as nb_del from selec_ap_pmsi where adm_ann in ('2021','2022') and ucd7 in (&scemblix_ucd) group by adm_ann,ucd7;
/*Colonne Total*/
create table res_pmsi_scemblix_2 as select distinct 'TOTAL' as adm_ann,ucd7,count(distinct ben_idt_ano)  as nb_patient,sum(nb_adm) as nb_adm,sum(nb_del) as nb_del from selec_ap_pmsi where adm_ann in ('2021','2022') and ucd7 in (&scemblix_ucd)  group by ucd7;
/*lignes Total*/
create table res_pmsi_scemblix_3 as select distinct adm_ann,'9999999' as ucd7 ,count(distinct ben_idt_ano)  as nb_patient,sum(nb_adm) as nb_adm,sum(nb_del) as nb_del from selec_ap_pmsi where adm_ann in ('2021','2022') and ucd7 in (&scemblix_ucd) group by adm_ann;
create table res_pmsi_scemblix_4 as select distinct 'TOTAL' as adm_ann, '9999999' as ucd7,count(distinct ben_idt_ano)  as nb_patient,sum(nb_adm) as nb_adm,sum(nb_del) as nb_del from selec_ap_pmsi where adm_ann in ('2021','2022') and ucd7 in (&scemblix_ucd);
quit;

data res_pmsi_scemblix;
format adm_ann $5.;
set res_pmsi_scemblix_1 res_pmsi_scemblix_2 res_pmsi_scemblix_3 res_pmsi_scemblix_4 ;
run;

proc sort data=res_pmsi_scemblix;by ucd7 adm_ann;run;

 /*Pour l'affichage de la sortie*/
 data out_pmsi_scemblix;
 set res_pmsi_scemblix;
 if nb_patient<11 then do;
 nb_patient=999999999;
 nb_adm=999999999;
 nb_del=999999999;
 end;
 run;
  
 /**********************************************************2 - EXTRACTION ET MISE EN FORME DONNEES PMSI MCO DGF EXTERNE 2021 et 2022*******************************************/
 /*
 ----------------------------------------------
Table RSFH externe (ACE)
� noter: code m�dicament correspondant retrouv� dans RSF-B = PHH Pharmacie hospitali�re � 100% => r�trocession (mais plus de la moiti� des RSFH sans RSFB associ�s donc pas de code de regroupement!!)
 ----------------------------------------------
 */
 
 /****************************Extraction*************************************/
 proc sql;
 create table selec_ap_pmsi_ace_&annee as select distinct c.nir_ano_17 as ben_nir_psa,h.ucd_cod,h.UCD_UCD_COD,h.COD_LES,h.exe_soi_dtd,sum(h.QUA) as nb_adm,count(distinct h.EXE_SOI_DTD) as nb_del
 from oravue.t_mco&an.fhstc h
  inner join oravue.t_mco&an.cstc c on (h.eta_num=c.eta_num and h.seq_num=c.seq_num)
 where c.nir_ret="0" and c.nai_ret="0" and c.sex_ret="0" and c.ias_ret="0" and c.ent_dat_ret="0"  and (ucd_cod in (&ucd7,&ucd13) or  ucd_ucd_cod in (&ucd7,&ucd13))
 group by c.nir_ano_17,h.ucd_cod,h.UCD_UCD_COD,h.COD_LES,h.EXE_SOI_DTD;
quit;

 proc sql;
 create table selec_ap_pmsi_ace_&annee1 as select distinct c.nir_ano_17 as ben_nir_psa,h.ucd_cod,h.UCD_UCD_COD,h.COD_LES,h.exe_soi_dtd,sum(h.QUA) as nb_adm,count(distinct h.EXE_SOI_DTD) as nb_del
 from oravue.t_mco&an1.fhstc h
 inner join oravue.t_mco&an1.cstc c on (h.eta_num=c.eta_num and h.seq_num=c.seq_num)
 where c.nir_ret="0" and c.nai_ret="0" and c.sex_ret="0" and c.ias_ret="0" and c.ent_dat_ret="0"  and (ucd_cod in (&ucd7,&ucd13) or  ucd_ucd_cod in (&ucd7,&ucd13))
 group by c.nir_ano_17,h.ucd_cod,h.UCD_UCD_COD,h.COD_LES,h.EXE_SOI_DTD;
quit;

/************Mise en forme des donn�es extraites**********************/
data selec_ap_pmsi_ace ;
set selec_ap_pmsi_ace_&annee selec_ap_pmsi_ace_&annee1;
adm_mois=put(month(datepart(exe_soi_dtd)),$2.);
adm_ann=put(year(datepart(exe_soi_dtd)),$4.);
ucd7=substr(ucd_cod,1,7);
run;

proc sql;
create table selec_ap_pmsi_ace as select distinct ben_nir_psa,adm_ann,adm_mois,ucd7,cod_les as indication,sum(nb_adm) as nb_adm,sum(nb_del) as nb_del from selec_ap_pmsi_ace
group by ben_nir_psa,adm_ann,adm_mois,ucd7,cod_les having (calculated nb_adm >0);
quit;
 
/*Ajout du ben_idt_ano dans la table AP DCIR (suppression des patients non retrouv�s dans IR_BEN_R)*/
proc sql;
drop table orauser.extract_ap_ben;
create table orauser.extract_ap_ben as select distinct ben_nir_psa from selec_ap_pmsi_ace;
quit;

proc sql;
create table extract_irbenr as select distinct b.ben_nir_psa, b.ben_rng_gem, b.ben_idt_ano from orauser.extract_ap_ben a inner join oravue.ir_ben_r b on (a.ben_nir_psa = b.ben_nir_psa);
quit;

proc sort data=extract_irbenr out=extract_irbenr_ nodupkey;by ben_nir_psa;run;

proc sql;
create table selec_ap_pmsi_ace as select a.* , b.ben_idt_ano from selec_ap_pmsi_ace a
inner join extract_irbenr_ b on (a.ben_nir_psa=b.ben_nir_psa);
quit; 

proc sql;
create table selec_ap_pmsi_ace as select distinct ben_idt_ano,adm_ann,adm_mois,ucd7,indication,sum(nb_adm) as nb_adm,sum(nb_del) as nb_del from selec_ap_pmsi_ace
group by ben_idt_ano,adm_ann,adm_mois,ucd7,indication;
quit;

/************Mise en forme des r�sultats: TRODELVY*******************/
proc sql;
/*par ann�e*/
create table res_pmsi_trodelvy_1 as select distinct adm_ann,ucd7,count(distinct ben_idt_ano) as nb_patient,sum(nb_adm) as nb_adm,sum(nb_del) as nb_del from selec_ap_pmsi_ace where adm_ann in ('2021','2022') and ucd7 in (&trodelvy_ucd) group by adm_ann,ucd7;
/*colonne Total*/
create table res_pmsi_trodelvy_2 as select distinct 'TOTAL' as adm_ann,ucd7,count(distinct ben_idt_ano) as nb_patient,sum(nb_adm) as nb_adm,sum(nb_del) as nb_del from selec_ap_pmsi_ace where adm_ann in ('2021','2022') and ucd7 in (&trodelvy_ucd) group by ucd7;
/*lignes Total*/
create table res_pmsi_trodelvy_3 as select distinct adm_ann,'9999999' as ucd7,count(distinct ben_idt_ano) as nb_patient,sum(nb_adm) as nb_adm,sum(nb_del) as nb_del from selec_ap_pmsi_ace where adm_ann in ('2021','2022') and ucd7 in (&trodelvy_ucd) group by adm_ann;
create table res_pmsi_trodelvy_4 as select distinct 'TOTAL' as adm_ann, '9999999' as ucd7,count(distinct ben_idt_ano) as nb_patient,sum(nb_adm) as nb_adm,sum(nb_del) as nb_del from selec_ap_pmsi_ace where adm_ann in ('2021','2022') and ucd7 in (&trodelvy_ucd);
quit;

data res_pmsi_trodelvy_ace;
format adm_ann $5.;
set res_pmsi_trodelvy_1 res_pmsi_trodelvy_2 res_pmsi_trodelvy_3 res_pmsi_trodelvy_4 ;
run;

proc sort data=res_pmsi_trodelvy_ace;by ucd7 adm_ann;run;

 /*Pour l'affichage de la sortie*/
 data out_pmsi_trodelvy_ace;
 set res_pmsi_trodelvy_ace;
 if nb_patient<11 then do;
 nb_patient=999999999;
 nb_adm=999999999;
 nb_del=999999999;
 end;
 run;
 
 /*******************Mise en forme des r�sultats: SCEMBLIX****************/
 proc sql;
 /*par ann�e*/
 create table res_pmsi_scemblix_1 as select distinct adm_ann,ucd7,count(distinct ben_idt_ano) as nb_patient,sum(nb_adm) as nb_adm,sum(nb_del) as nb_del from selec_ap_pmsi_ace where adm_ann in ('2021','2022') and ucd7 in (&scemblix_ucd) group by adm_ann,ucd7;
 /*Colonne Total*/
 create table res_pmsi_scemblix_2 as select distinct 'TOTAL' as adm_ann,ucd7 ,count(distinct ben_idt_ano) as nb_patient,sum(nb_adm) as nb_adm,sum(nb_del) as nb_del from selec_ap_pmsi_ace  where adm_ann in ('2021','2022') and ucd7 in (&scemblix_ucd) group by ucd7;
 /*lignes Total*/
 create table res_pmsi_scemblix_3 as select distinct adm_ann,'9999999' as ucd7 ,count(distinct ben_idt_ano) as nb_patient,sum(nb_adm) as nb_adm,sum(nb_del) as nb_del from selec_ap_pmsi_ace where adm_ann in ('2021','2022') and ucd7 in (&scemblix_ucd) group by adm_ann;
 create table res_pmsi_scemblix_4 as select distinct 'TOTAL' as adm_ann, '9999999' as ucd7 ,count(distinct ben_idt_ano) as nb_patient,sum(nb_adm) as nb_adm,sum(nb_del) as nb_del from selec_ap_pmsi_ace where adm_ann in ('2021','2022') and ucd7 in (&scemblix_ucd);
 quit;
 
 data res_pmsi_scemblix_ace;
 format adm_ann $5.;
 set res_pmsi_scemblix_1 res_pmsi_scemblix_2 res_pmsi_scemblix_3 res_pmsi_scemblix_4 ;
 run;
 
 proc sort data=res_pmsi_scemblix_ace;by ucd7 adm_ann;run;
 
  /*Pour l'affichage de la sortie*/
  data out_pmsi_scemblix_ace;
  set res_pmsi_scemblix_ace;
  if nb_patient<11 then do;
  nb_patient=999999999;
  nb_adm=999999999;
  nb_del=999999999;
  end;
 run;
 
 /**********************************************************3 - EXTRACTION ET MISE EN FORME DONNEES PMSI MCO OQN HOSPIT 2021 et 2022*******************************************/
 /*
 ----------------------------------------------
Table RSFH 
� noter: code m�dicament correspondant retrouv� dans RSF-B = PHX Pharmacie sous ATU s�jour 
 ----------------------------------------------
*/

/****************************Extraction*************************************/ 
 proc sql;
create table selec_ap_pmsi_oqn_&annee as select distinct c.nir_ano_17 as ben_nir_psa,h.COD_UCD,h.UCD_UCD_COD,h.COD_LES,h.exe_soi_dtd,sum(h.QUA_COD) as nb_adm,count(distinct h.EXE_SOI_DTD) as nb_del
from oravue.t_mco&an.fh h
inner join oravue.t_mco&an.c c on (h.eta_num=c.eta_num and h.rsa_num=c.rsa_num)
where c.NIR_RET='0' and c.NAI_RET='0' and c.SEX_RET='0' and c.SEJ_RET='0' and c.FHO_RET='0' and c.PMS_RET='0' and c.DAT_RET='0' and c.COH_NAI_RET='0' and c.COH_SEX_RET='0' and (cod_ucd in (&ucd7,&ucd13) or  ucd_ucd_cod in (&ucd7,&ucd13))
group by c.nir_ano_17,h.COD_UCD,h.UCD_UCD_COD,h.COD_LES,h.EXE_SOI_DTD;
quit;

proc sql;
create table selec_ap_pmsi_oqn_&annee1 as select distinct c.nir_ano_17 as ben_nir_psa,h.COD_UCD,h.UCD_UCD_COD,h.COD_LES,h.exe_soi_dtd,sum(h.QUA_COD) as nb_adm,count(distinct h.EXE_SOI_DTD) as nb_del
from oravue.t_mco&an1.fh h
inner join oravue.t_mco&an1.c c on (h.eta_num=c.eta_num and h.rsa_num=c.rsa_num)
where c.NIR_RET='0' and c.NAI_RET='0' and c.SEX_RET='0' and c.SEJ_RET='0' and c.FHO_RET='0' and c.PMS_RET='0' and c.DAT_RET='0' and c.COH_NAI_RET='0' and c.COH_SEX_RET='0' and (cod_ucd in (&ucd7,&ucd13) or  ucd_ucd_cod in (&ucd7,&ucd13))
group by c.nir_ano_17,h.COD_UCD,h.UCD_UCD_COD,h.COD_LES,h.EXE_SOI_DTD;
quit;

/************Mise en forme des donn�es extraites**********************/
data selec_ap_pmsi_oqn ;
set selec_ap_pmsi_oqn_&annee selec_ap_pmsi_oqn_&annee1;
adm_mois=put(month(datepart(exe_soi_dtd)),$2.);
adm_ann=put(year(datepart(exe_soi_dtd)),$4.);
ucd7=substr(cod_ucd,1,7);
run;

proc sql;
create table selec_ap_pmsi_oqn as select distinct ben_nir_psa,adm_ann,adm_mois,ucd7,cod_les as indication,sum(nb_adm) as nb_adm,sum(nb_del) as nb_del from selec_ap_pmsi_oqn
group by ben_nir_psa,adm_ann,adm_mois,ucd7,cod_les having (calculated nb_adm >0);
quit;
 
/*Ajout du ben_idt_ano dans la table AP DCIR (suppression des patients non retrouv�s dans IR_BEN_R)*/
proc sql;
drop table orauser.extract_ap_ben;
create table orauser.extract_ap_ben as select distinct ben_nir_psa from selec_ap_pmsi_oqn;
quit;

proc sql;
create table extract_irbenr as select distinct b.ben_nir_psa, b.ben_rng_gem, b.ben_idt_ano from orauser.extract_ap_ben a inner join oravue.ir_ben_r b on (a.ben_nir_psa = b.ben_nir_psa);
quit;

proc sort data=extract_irbenr out=extract_irbenr_ nodupkey;by ben_nir_psa;run;

proc sql;
create table selec_ap_pmsi_oqn as select a.* , b.ben_idt_ano from selec_ap_pmsi_oqn a
inner join extract_irbenr_ b on (a.ben_nir_psa=b.ben_nir_psa);
quit; 

proc sql;
create table selec_ap_pmsi_oqn as select distinct ben_idt_ano,adm_ann,adm_mois,ucd7,indication,sum(nb_adm) as nb_adm,sum(nb_del) as nb_del from selec_ap_pmsi_oqn
group by ben_idt_ano,adm_ann,adm_mois,ucd7,indication;
quit;

/************Mise en forme des r�sultats: TRODELVY*******************/
proc sql;
/*par ann�e*/
create table res_pmsi_trodelvy_1 as select distinct adm_ann,ucd7,count(distinct ben_idt_ano) as nb_patient,sum(nb_adm) as nb_adm,sum(nb_del) as nb_del from selec_ap_pmsi_oqn where adm_ann in ('2021','2022') and ucd7 in (&trodelvy_ucd) group by adm_ann,ucd7;
/*colonne Total*/
create table res_pmsi_trodelvy_2 as select distinct 'TOTAL' as adm_ann,ucd7,count(distinct ben_idt_ano)  as nb_patient,sum(nb_adm) as nb_adm,sum(nb_del) as nb_del from selec_ap_pmsi_oqn where adm_ann in ('2021','2022') and ucd7 in (&trodelvy_ucd) group by ucd7;
/*lignes Total*/
create table res_pmsi_trodelvy_3 as select distinct adm_ann,'9999999' as ucd7,count(distinct ben_idt_ano)  as nb_patient,sum(nb_adm) as nb_adm,sum(nb_del) as nb_del from selec_ap_pmsi_oqn where adm_ann in ('2021','2022') and ucd7 in (&trodelvy_ucd) group by adm_ann;
create table res_pmsi_trodelvy_4 as select distinct 'TOTAL' as adm_ann, '9999999' as ucd7,count(distinct ben_idt_ano)  as nb_patient,sum(nb_adm) as nb_adm,sum(nb_del) as nb_del from selec_ap_pmsi_oqn where adm_ann in ('2021','2022') and ucd7 in (&trodelvy_ucd);
quit;

data res_pmsi_trodelvy_oqn;
format adm_ann $5.;
set res_pmsi_trodelvy_1 res_pmsi_trodelvy_2 res_pmsi_trodelvy_3 res_pmsi_trodelvy_4 ;
run;

proc sort data=res_pmsi_trodelvy_oqn;by ucd7 adm_ann ;run;

 /*Pour l'affichage de la sortie*/
 data out_pmsi_trodelvy_oqn;
 set res_pmsi_trodelvy_oqn;
 if nb_patient<11 then do;
 nb_patient=999999999;
 nb_adm=999999999;
 nb_del=999999999;
 end;
 run;
 
 /*******************Mise en forme des r�sultats: SCEMBLIX****************/
 proc sql;
 /*par ann�e*/
 create table res_pmsi_scemblix_1 as select distinct adm_ann,ucd7,count(distinct ben_idt_ano) as nb_patient,sum(nb_adm) as nb_adm,sum(nb_del) as nb_del from selec_ap_pmsi_oqn where adm_ann in ('2021','2022') and ucd7 in (&scemblix_ucd) group by adm_ann,ucd7;
 /*Colonne Total*/
 create table res_pmsi_scemblix_2 as select distinct 'TOTAL' as adm_ann,ucd7,count(distinct ben_idt_ano) as nb_patient,sum(nb_adm) as nb_adm,sum(nb_del) as nb_del from selec_ap_pmsi_oqn  where adm_ann in ('2021','2022') and ucd7 in (&scemblix_ucd) group by ucd7;
 /*lignes Total*/
 create table res_pmsi_scemblix_3 as select distinct adm_ann,'9999999' as ucd7,count(distinct ben_idt_ano) as nb_patient,sum(nb_adm) as nb_adm,sum(nb_del) as nb_del from selec_ap_pmsi_oqn where adm_ann in ('2021','2022') and ucd7 in (&scemblix_ucd) group by adm_ann;
 create table res_pmsi_scemblix_4 as select distinct 'TOTAL' as adm_ann, '9999999' as ucd7 ,count(distinct ben_idt_ano) as nb_patient,sum(nb_adm) as nb_adm,sum(nb_del) as nb_del from selec_ap_pmsi_oqn where adm_ann in ('2021','2022') and ucd7 in (&scemblix_ucd);
 quit;
 
 data res_pmsi_scemblix_oqn;
 format adm_ann $5.;
 set res_pmsi_scemblix_1 res_pmsi_scemblix_2 res_pmsi_scemblix_3 res_pmsi_scemblix_4 ;
 run;
 
 proc sort data=res_pmsi_scemblix_oqn;by ucd7 adm_ann;run;
 
  /*Pour l'affichage de la sortie*/
  data out_pmsi_scemblix_oqn;
  set res_pmsi_scemblix_oqn;
  if nb_patient<11 then do;
  nb_patient=999999999;
  nb_adm=999999999;
  nb_del=999999999;
  end;
 run;

/**********************************************************4 - EXTRACTION ET MISE EN FORME DONNEES DCIR 2021 et 2022*******************************************/
/*
----------------------------------------------
Table UCD
Table PHA: Apr�s v�rification, rien dans cette table en 2021 et 2022 pour les 2 m�dicaments => on met la partie correspondante en commentaire
----------------------------------------------
*/

/**********************Extraction**********************************/
%MACRO CREA_TABLE ( FLX_DIS_DTD ) ;
%PUT ### Mois de flux ( &FLX_DIS_DTD ) D�but %SYSFUNC( datetime(),datetime. ) ;

proc sql;
create table extract_ucd_ as 
SELECT a.exe_soi_dtd, a.exe_soi_dtf,a.ben_nir_psa, a.ben_rng_gem, a.prs_nat_ref,b.ucd_top_ucd,b.ucd_ucd_cod,b.ucd_dlv_nbr
/*,c.ETE_IND_TAA, c.ETE_CAT_COD, c.PRS_PPU_SEC, c.ETB_EXE_FIN, c.mdt_cod,c.ete_ghs_num*/
from oravue.er_prs_f a 
inner join oravue.er_ucd_f b on
	a.FLX_DIS_DTD = b.FLX_DIS_DTD and a.FLX_TRT_DTD = b.FLX_TRT_DTD and a.FLX_EMT_TYP = b.FLX_EMT_TYP and a.FLX_EMT_NUM = b.FLX_EMT_NUM and a.FLX_EMT_ORD = b.FLX_EMT_ORD and a.ORG_CLE_NUM = b.ORG_CLE_NUM and
  	a.DCT_ORD_NUM = b.DCT_ORD_NUM and a.PRS_ORD_NUM = b.PRS_ORD_NUM and a.REM_TYP_AFF = b.REM_TYP_AFF
/*left join oravue.ER_ETE_F c on 		  
	a.FLX_DIS_DTD = c.FLX_DIS_DTD and a.FLX_TRT_DTD = c.FLX_TRT_DTD and a.FLX_EMT_TYP = c.FLX_EMT_TYP and a.FLX_EMT_NUM = c.FLX_EMT_NUM and a.FLX_EMT_ORD = c.FLX_EMT_ORD and a.ORG_CLE_NUM = c.ORG_CLE_NUM and
  	a.DCT_ORD_NUM = c.DCT_ORD_NUM and a.PRS_ORD_NUM = c.PRS_ORD_NUM and a.REM_TYP_AFF = c.REM_TYP_AFF*/
where a.FLX_DIS_DTD="&flx_dis_dtd:0:0:0"dt
      AND a.BEN_CDI_NIR NOT IN ('01','05','06','08','09','11','12','13','14','15','48','46','47')  /*exclusion des nir fictifs*/
      and a.exe_soi_dtd between ("&date_exe_deb:0:0:0"dt) and ("&date_exe_fin:0:0:0"dt) /*s�lection p�riode �tude*/
      and a.DPN_QLF not in (71) /*exclusion de l'activit� des h�pitaux publics transmis pour information => activit� r�cup�r�e dans le PMSI */
      and b.ucd_ucd_cod in (&ucd13,&ucd7) ; /*Attention codage en UCD7 normalement mais sur 13 caract�re avec des 0 devant les 7 caract�res d'int�r�t*/
quit;

/*proc sql;
create table extract_pha_ as 
SELECT a.exe_soi_dtd, a.exe_soi_dtf,a.ben_nir_psa, a.ben_rng_gem, a.prs_nat_ref,a.CPL_MAJ_TOP,a.PRS_ACT_QTE,
b.pha_prs_c13,b.pha_prs_ide,b.pha_ide_cpl,b.pha_act_qsn,b.pha_dec_qsu
from oravue.er_prs_f a 
inner join oravue.er_pha_f b on
	a.FLX_DIS_DTD = b.FLX_DIS_DTD and a.FLX_TRT_DTD = b.FLX_TRT_DTD and a.FLX_EMT_TYP = b.FLX_EMT_TYP and a.FLX_EMT_NUM = b.FLX_EMT_NUM and a.FLX_EMT_ORD = b.FLX_EMT_ORD and a.ORG_CLE_NUM = b.ORG_CLE_NUM and
  	a.DCT_ORD_NUM = b.DCT_ORD_NUM and a.PRS_ORD_NUM = b.PRS_ORD_NUM and a.REM_TYP_AFF = b.REM_TYP_AFF
where a.FLX_DIS_DTD="&flx_dis_dtd:0:0:0"dt
      AND a.BEN_CDI_NIR NOT IN ('01','05','06','08','09','11','12','13','14','15','48','46','47') 
      and a.exe_soi_dtd between ("&date_exe_deb:0:0:0"dt) and ("&date_exe_fin:0:0:0"dt) 
      and a.DPN_QLF not in (71) 
      and (b.pha_prs_c13 in (&cip13,&cip7) or b.pha_prs_ide in (&cip13,&cip7)); 
quit;*/

%PUT ### CREA_TABLE ( &FLX_DIS_DTD ) Fin %SYSFUNC( datetime(),datetime. ) ;
%MEND CREA_TABLE ;

/***  Macro Compilation des tables mensuelles (proc append)****/
%MACRO COMPILATION() ;
%IF %SYSFUNC(exist(extract_ucd)) eq 0 %THEN %DO ;
proc datasets nolist memtype=data;
change extract_ucd_=extract_ucd;
quit;
%END ;
%ELSE %DO;
proc append base=extract_ucd data=extract_ucd_ FORCE;run;
proc delete data=extract_ucd_;run;
%END;

/*%IF %SYSFUNC(exist(extract_pha)) eq 0 %THEN %DO ;
proc datasets nolist memtype=data;
change extract_pha_=extract_pha;
quit;
%END ;
%ELSE %DO;
proc append base=extract_pha data=extract_pha_ FORCE;run;
proc delete data=extract_pha_;run;
%END;*/
%MEND COMPILATION ;

/*** Ex�cution pour chaque mois de flux***/
%MACRO TAB_PRS( Date_DEB= ,Date_FIN= ) ;

/*DEFINITION DES PARAMETRES*/
%LET AN_DEB = %SUBSTR( &DATE_DEB,1,4 ) ;
%LET mois_deb = %SUBSTR( &DATE_DEB,5,2 ) ;
%LET AN_FIN = %SUBSTR( &DATE_FIN,1,4 ) ;
%LET mois_fin = %SUBSTR( &DATE_FIN,5,2 ) ;

/*dates de soin en format SAS*/
%LET EXE_DEB = %SYSFUNC( mdy( &mois_deb,1,&AN_DEB )) ;
%LET EXE_FIN0 = %SYSFUNC( mdy( &mois_fin,1,&an_fin )) ; 
%LET EXE_FIN = %EVAL( %SYSFUNC( intnx( month,&EXE_FIN0, 1 ))-1 ) ; /*pour aller jusqu'au dernier jour du mois de la date de fin donn�e*/

/*dates de soin en format date9*/
%LET DATE_EXE_DEB = %SYSFUNC( putn( &EXE_DEB,date9. )) ;
%LET DATE_EXE_FIN = %SYSFUNC( putn( &EXE_FIN,date9. )) ;
 
 /*suppression de la table compil�e des mois extraits si ce n'est pas la 1�re ex�cution*/
%IF %SYSFUNC(exist(extract_ucd)) %THEN %DO;
 	PROC DELETE DATA=extract_ucd;
	RUN ;
%END;

/*%IF %SYSFUNC(exist(extract_pha)) %THEN %DO;
 	PROC DELETE DATA=extract_pha;
	RUN ;
%END;*/

/*BOUCLE MENSUELLE DE SELECTION DES PRESTATIONS*/
%LET iterDeb = 1; 
%LET iterFin = %SYSFUNC( intck( month,&EXE_DEB,&EXE_FIN )) ;/*Nombre de mois entre le d�but et la fin des soins */

/*BOUCLE SUR LES MOIS*/
	%DO mois=&iterDeb %TO &iterFin + 7 ;/*flux sur p�riode + 6 mois*/
	%LET FLX_DIS_DTD = %SYSFUNC( intnx( MONTH,&EXE_DEB,&mois ), date9. ) ;
	%LET an = %SYSFUNC( year( %SYSFUNC( intnx( MONTH,&EXE_DEB,&mois-1 )))) ;
				 
	/*affichage dans le journal*/
	%PUT "Valeurs des param�tres" ;
	%PUT DATE_EXE_DEB = &DATE_EXE_DEB ;
	%PUT DATE_EXE_FIN = &DATE_EXE_FIN ;
	%PUT an = &an ;
	%PUT FLX_DIS_DTD = &FLX_DIS_DTD ;

	%CREA_TABLE(&FLX_DIS_DTD) ;
	%COMPILATION() ;			
%END ;

%MEND TAB_PRS;

/*lancement de la macro*/
OPTIONS MPRINT ;
 %TAB_PRS(Date_DEB = &date_debm,Date_FIN = &date_finm) ;
 
/************Mise en forme des donn�es extraites et harmonisation avec les donn�es du PMSI**********************/
proc sql;
create table selec_ap_dcir as select distinct ben_nir_psa,ben_rng_gem,exe_soi_dtd,ucd_ucd_cod,ucd_top_ucd,sum(ucd_dlv_nbr) as nb_adm,count(distinct exe_soi_dtd) as nb_del from extract_ucd 
group by ben_nir_psa,ben_rng_gem,exe_soi_dtd,ucd_ucd_cod,ucd_top_ucd having (calculated nb_adm >0);
quit;
 
data selec_ap_dcir;
set selec_ap_dcir;
ucd7=substr(ucd_ucd_cod,7,7);
adm_mois=put(month(datepart(exe_soi_dtd)),$2.);
adm_ann=put(year(datepart(exe_soi_dtd)),$4.);
drop exe_soi_dtd ucd_ucd_cod;
run;

/*Ajout du ben_idt_ano dans la table AP DCIR (suppression des patients non retrouv�s dans IR_BEN_R)*/
proc sql;
drop table orauser.extract_ap_ben;
CREATE TABLE orauser.extract_ap_ben AS SELECT distinct ben_nir_psa,ben_rng_gem from selec_ap_dcir;
quit;
  
proc sql;
CREATE table extract_irbenr_dcir AS SELECT distinct b.ben_nir_psa, b.BEN_RNG_GEM, b.ben_idt_ano FROM orauser.extract_ap_ben a inner JOIN ORAVUE.IR_BEN_R b ON (a.ben_nir_psa = b.BEN_NIR_PSA and a.ben_rng_gem = b.ben_rng_gem);
quit;

proc sort data=extract_irbenr_dcir nodupkey;by ben_nir_psa ben_rng_gem;run;

proc sql;
create table selec_ap_dcir as select a.* , b.ben_idt_ano from selec_ap_dcir  a
inner join extract_irbenr_dcir b on (a.ben_nir_psa=b.ben_nir_psa and a.ben_rng_gem=b.ben_rng_gem);
quit; 

proc sql;
create table selec_ap_dcir as select distinct ben_idt_ano,adm_ann,adm_mois,ucd7,ucd_top_ucd,sum(nb_adm) as nb_adm,sum(nb_del) as nb_del from selec_ap_dcir 
group by ben_idt_ano,adm_ann,adm_mois,ucd7,ucd_top_ucd;
quit;

data selec_ap_dcir_hospit selec_ap_dcir_retro;
set selec_ap_dcir;
if ucd_top_ucd=1 then output selec_ap_dcir_hospit;
else output selec_ap_dcir_retro;
run;
 
 /************Mise en forme des r�sultats: TRODELVY HOSPIT*******************/
 proc sql;
 /*par ann�e*/
 create table res_dcir_trodelvy_1 as select distinct adm_ann,ucd7,count(distinct ben_idt_ano) as nb_patient, sum(nb_adm) as nb_adm,sum(nb_del) as nb_del from selec_ap_dcir_hospit where adm_ann in ('2021','2022') and ucd7 in (&trodelvy_ucd) group by adm_ann,ucd7;
 /*colonne Total*/
 create table res_dcir_trodelvy_2 as select distinct 'TOTAL' as adm_ann,ucd7,count(distinct ben_idt_ano) as nb_patient, sum(nb_adm) as nb_adm,sum(nb_del) as nb_del from selec_ap_dcir_hospit where adm_ann in ('2021','2022') and ucd7 in (&trodelvy_ucd) group by ucd7;
 /*lignes Total*/
 create table res_dcir_trodelvy_3 as select distinct adm_ann,'9999999' as ucd7,count(distinct ben_idt_ano) as nb_patient, sum(nb_adm) as nb_adm,sum(nb_del) as nb_del from selec_ap_dcir_hospit where adm_ann in ('2021','2022') and ucd7 in (&trodelvy_ucd) group by adm_ann;
 create table res_dcir_trodelvy_4 as select distinct 'TOTAL' as adm_ann, '9999999' as ucd7,count(distinct ben_idt_ano) as nb_patient, sum(nb_adm) as nb_adm,sum(nb_del) as nb_del from selec_ap_dcir_hospit where adm_ann in ('2021','2022') and ucd7 in (&trodelvy_ucd) ;
 quit;
 
 data res_dcir_trodelvy_hospit;
 format adm_ann $5.;
 set res_dcir_trodelvy_1 res_dcir_trodelvy_2 res_dcir_trodelvy_3 res_dcir_trodelvy_4 ;
 run;
 
 proc sort data=res_dcir_trodelvy_hospit;by ucd7 adm_ann;run;

 /*Pour l'affichage de la sortie*/
 data out_dcir_trodelvy_hospit;
 set res_dcir_trodelvy_hospit;
 if nb_patient<11 then do;
 nb_patient=999999999;
 nb_adm=999999999;
 nb_del=999999999;
 end;
 run;
 
  /************Mise en forme des r�sultats: TRODELVY RETROCESSION*******************/
  proc sql;
  /*par ann�e*/
  create table res_dcir_trodelvy_1 as select distinct adm_ann,ucd7,count(distinct ben_idt_ano) as nb_patient,  sum(nb_adm) as nb_adm,sum(nb_del) as nb_del from selec_ap_dcir_retro where adm_ann in ('2021','2022') and ucd7 in (&trodelvy_ucd) group by adm_ann,ucd7;
  /*colonne Total*/
  create table res_dcir_trodelvy_2 as select distinct 'TOTAL' as adm_ann,ucd7,count(distinct ben_idt_ano) as nb_patient,  sum(nb_adm) as nb_adm,sum(nb_del) as nb_del from selec_ap_dcir_retro where adm_ann in ('2021','2022') and ucd7 in (&trodelvy_ucd) group by ucd7;
  /*lignes Total*/
  create table res_dcir_trodelvy_3 as select distinct adm_ann,'9999999' as ucd7,count(distinct ben_idt_ano) as nb_patient,  sum(nb_adm) as nb_adm,sum(nb_del) as nb_del from selec_ap_dcir_retro where adm_ann in ('2021','2022') and ucd7 in (&trodelvy_ucd) group by adm_ann;
  create table res_dcir_trodelvy_4 as select distinct 'TOTAL' as adm_ann, '9999999' as ucd7,count(distinct ben_idt_ano) as nb_patient,  sum(nb_adm) as nb_adm,sum(nb_del) as nb_del from selec_ap_dcir_retro where adm_ann in ('2021','2022') and ucd7 in (&trodelvy_ucd) ;
  quit;
  
  data res_dcir_trodelvy_retro;
  format adm_ann $5.;
  set res_dcir_trodelvy_1 res_dcir_trodelvy_2 res_dcir_trodelvy_3 res_dcir_trodelvy_4 ;
  run;
  
  proc sort data=res_dcir_trodelvy_retro;by ucd7 adm_ann ;run;
 
  /*Pour l'affichage de la sortie*/
  data out_dcir_trodelvy_retro;
  set res_dcir_trodelvy_retro;
  if nb_patient<11 then do;
  nb_patient=999999999;
  nb_adm=999999999;
  nb_del=999999999;
  end;
 run;
 
/************Mise en forme des r�sultats: SCEMBLIX HOSPIT*******************/
 proc sql;
 /*par ann�e*/
 create table res_dcir_scemblix_1 as select distinct adm_ann,ucd7,count(distinct ben_idt_ano) as nb_patient, sum(nb_adm) as nb_adm,sum(nb_del) as nb_del from selec_ap_dcir_hospit where adm_ann in ('2021','2022') and ucd7 in (&scemblix_ucd) group by adm_ann,ucd7;
 /*colonne Total*/
 create table res_dcir_scemblix_2 as select distinct 'TOTAL' as adm_ann,ucd7,count(distinct ben_idt_ano) as nb_patient, sum(nb_adm) as nb_adm,sum(nb_del) as nb_del from selec_ap_dcir_hospit where adm_ann in ('2021','2022') and ucd7 in (&scemblix_ucd) group by ucd7;
 /*lignes Total*/
 create table res_dcir_scemblix_3 as select distinct adm_ann,'9999999' as ucd7,count(distinct ben_idt_ano) as nb_patient, sum(nb_adm) as nb_adm,sum(nb_del) as nb_del from selec_ap_dcir_hospit where adm_ann in ('2021','2022') and ucd7 in (&scemblix_ucd) group by adm_ann;
 create table res_dcir_scemblix_4 as select distinct 'TOTAL' as adm_ann, '9999999' as ucd7,count(distinct ben_idt_ano) as nb_patient, sum(nb_adm) as nb_adm,sum(nb_del) as nb_del from selec_ap_dcir_hospit where adm_ann in ('2021','2022') and ucd7 in (&scemblix_ucd) ;
 quit;
 
 data res_dcir_scemblix_hospit;
 format adm_ann $5.;
 set res_dcir_scemblix_1 res_dcir_scemblix_2 res_dcir_scemblix_3 res_dcir_scemblix_4 ;
 run;
 
 proc sort data=res_dcir_scemblix_hospit;by ucd7 adm_ann ;run;
 
 /*Pour l'affichage de la sortie*/
 data out_dcir_scemblix_hospit;
 set res_dcir_scemblix_hospit;
 if nb_patient<11 then do;
 nb_patient=999999999;
 nb_adm=999999999;
 nb_del=999999999;
 end;
 run;
 
 /************Mise en forme des r�sultats: SCEMBLIX RETRO*******************/
  proc sql;
  /*par ann�e*/
  create table res_dcir_scemblix_1 as select distinct adm_ann,ucd7,count(distinct ben_idt_ano) as nb_patient,  sum(nb_adm) as nb_adm,sum(nb_del) as nb_del from selec_ap_dcir_retro where adm_ann in ('2021','2022') and ucd7 in (&scemblix_ucd) group by adm_ann,ucd7;
  /*colonne Total*/
  create table res_dcir_scemblix_2 as select distinct 'TOTAL' as adm_ann,ucd7,count(distinct ben_idt_ano) as nb_patient,  sum(nb_adm) as nb_adm,sum(nb_del) as nb_del from selec_ap_dcir_retro where adm_ann in ('2021','2022') and ucd7 in (&scemblix_ucd) group by ucd7;
  /*lignes Total*/
  create table res_dcir_scemblix_3 as select distinct adm_ann,'9999999' as ucd7,count(distinct ben_idt_ano) as nb_patient,  sum(nb_adm) as nb_adm,sum(nb_del) as nb_del from selec_ap_dcir_retro where adm_ann in ('2021','2022') and ucd7 in (&scemblix_ucd) group by adm_ann;
  create table res_dcir_scemblix_4 as select distinct 'TOTAL' as adm_ann, '9999999' as ucd7,count(distinct ben_idt_ano) as nb_patient,  sum(nb_adm) as nb_adm,sum(nb_del) as nb_del from selec_ap_dcir_retro where adm_ann in ('2021','2022') and ucd7 in (&scemblix_ucd) ;
  quit;
  
  data res_dcir_scemblix_retro;
  format adm_ann $5.;
  set res_dcir_scemblix_1 res_dcir_scemblix_2 res_dcir_scemblix_3 res_dcir_scemblix_4 ;
  run;
  
  proc sort data=res_dcir_scemblix_retro;by ucd7 adm_ann;run;
  
  /*Pour l'affichage de la sortie*/
  data out_dcir_scemblix_retro;
  set res_dcir_scemblix_retro;
  if nb_patient<11 then do;
  nb_patient=999999999;
  nb_adm=999999999;
  nb_del=999999999;
  end;
 run;

/**********************************************************5 - MISE EN FORME DONNEES PMSI ET DCIR******************************************************************************************************/

/*=> Choix: Donn�es DCIR pour r�trocession et hospitalisation priv� / Donn�es PMSI pour hospitalisation publique*/
data selec_ap;
set selec_ap_dcir selec_ap_pmsi (in=a);
if a then ucd_top_ucd=2;
drop indication;
run;
 
/************Mise en forme des r�sultats: TRODELVY*******************/
 proc sql;
 /*par ann�e*/
 create table res_trodelvy_1 as select distinct adm_ann,ucd7,ucd_top_ucd ,count(distinct ben_idt_ano) as nb_patient, sum(nb_adm) as nb_adm,sum(nb_del) as nb_del  from selec_ap where adm_ann in ('2021','2022') and ucd7 in (&trodelvy_ucd) group by adm_ann,ucd7,ucd_top_ucd;
 /*colonne Total*/
 create table res_trodelvy_2 as select distinct 'TOTAL' as adm_ann,ucd7,ucd_top_ucd ,count(distinct ben_idt_ano) as nb_patient, sum(nb_adm) as nb_adm,sum(nb_del) as nb_del from selec_ap where adm_ann in ('2021','2022') and ucd7 in (&trodelvy_ucd) group by ucd7, ucd_top_ucd;
 /*lignes Total*/
 create table res_trodelvy_3 as select distinct adm_ann,'9999999' as ucd7,5 as ucd_top_ucd ,count(distinct ben_idt_ano) as nb_patient, sum(nb_adm) as nb_adm,sum(nb_del) as nb_del  from selec_ap where adm_ann in ('2021','2022') and ucd7 in (&trodelvy_ucd) group by adm_ann;
 create table res_trodelvy_4 as select distinct 'TOTAL' as adm_ann, '9999999' as ucd7,5 as ucd_top_ucd ,count(distinct ben_idt_ano) as nb_patient, sum(nb_adm) as nb_adm,sum(nb_del) as nb_del  from selec_ap where adm_ann in ('2021','2022') and ucd7 in (&trodelvy_ucd) ;
 quit;
 
 data res_trodelvy;
 format adm_ann $5.;
 set res_trodelvy_1 res_trodelvy_2 res_trodelvy_3 res_trodelvy_4 ;
 run;
 
 proc sort data=res_trodelvy;by ucd7 adm_ann ucd_top_ucd;run;

 /*Pour l'affichage de la sortie*/
 data out_trodelvy;
 set res_trodelvy;
 if nb_patient<11 then do;
 nb_patient=999999999;
 nb_adm=999999999;
 nb_del=999999999;
 end;
 run;

/************Mise en forme des r�sultats: SCEMBLIX*******************/
 proc sql;
 /*par ann�e*/
 create table res_scemblix_1 as select distinct adm_ann,ucd7,ucd_top_ucd ,count(distinct ben_idt_ano) as nb_patient, sum(nb_adm) as nb_adm,sum(nb_del) as nb_del from selec_ap where adm_ann in ('2021','2022') and ucd7 in (&scemblix_ucd) group by adm_ann,ucd7,ucd_top_ucd;
 /*colonne Total*/
 create table res_scemblix_2 as select distinct 'TOTAL' as adm_ann,ucd7,ucd_top_ucd ,count(distinct ben_idt_ano) as nb_patient, sum(nb_adm) as nb_adm,sum(nb_del) as nb_del from selec_ap where adm_ann in ('2021','2022') and ucd7 in (&scemblix_ucd) group by ucd7, ucd_top_ucd;
 /*lignes Total*/
 create table res_scemblix_3 as select distinct adm_ann,'9999999' as ucd7,5 as ucd_top_ucd ,count(distinct ben_idt_ano) as nb_patient, sum(nb_adm) as nb_adm,sum(nb_del) as nb_del from selec_ap where adm_ann in ('2021','2022') and ucd7 in (&scemblix_ucd) group by adm_ann;
 create table res_scemblix_4 as select distinct 'TOTAL' as adm_ann, '9999999' as ucd7,5 as ucd_top_ucd ,count(distinct ben_idt_ano) as nb_patient, sum(nb_adm) as nb_adm,sum(nb_del) as nb_del from selec_ap where adm_ann in ('2021','2022') and ucd7 in (&scemblix_ucd) ;
 quit;
 
 data res_scemblix;
 format adm_ann $5.;
 set res_scemblix_1 res_scemblix_2 res_scemblix_3 res_scemblix_4 ;
 run;
 
 proc sort data=res_scemblix;by ucd7 adm_ann ucd_top_ucd;run;
 
 /*Pour l'affichage de la sortie*/
 data out_scemblix;
 set res_scemblix;
 if nb_patient<11 then do;
 nb_patient=999999999;
 nb_adm=999999999;
 nb_del=999999999;
 end;
 run;

/**********************************************************6 - RESTITUTION DES RESULTATS******************************************************************************************************/
Footnote "A0"X;
ods excel file="./res_acces_precoce.xlsx" ;

/**************TRODELVY****************/
ods excel options(start_at="B2" embedded_titles="yes" embedded_footnotes='yes'  sheet_name="TRODELVY" sheet_interval='none');

proc report data=out_pmsi_trodelvy nowd  split='*' missing; 
columns ucd7 adm_ann, (nb_patient nb_adm nb_del) ;
define adm_ann/across "Ann�e d'administration"  order=data;
define ucd7/group format=$ucd. "Code UCD" order=data;
define nb_patient/ "Nb patients" format=commaxa.;
define nb_adm/ "Nb administr�" format=commaxd.;
define nb_del/ "Nb de d�livrances" format=commaxa.;
Title1 "TRODELVY administr� lors d'un s�jour hospitalier dans un ES public (donn�es PMSI)";
Title2 "Date de traitement: &datetrt";
run;

proc report data=out_pmsi_trodelvy_oqn nowd  split='*' missing; 
columns ucd7 adm_ann, (nb_patient nb_adm nb_del) ;
define adm_ann/across "Ann�e d'administration"  order=data;
define ucd7/group format=$ucd. "Code UCD" order=data;
define nb_patient/ "Nb patients" format=commaxa.;
define nb_adm/ "Nb administr�" format=commaxd.;
define nb_del/ "Nb de d�livrances" format=commaxa.;
Title1 "TRODELVY administr� lors d'un s�jour hospitalier dans un ES priv� (donn�es PMSI)";
Title2 "Date de traitement: &datetrt";
run;

proc report data=out_dcir_trodelvy_hospit nowd  split='*' missing; 
columns ucd7 adm_ann, (nb_patient nb_adm nb_del) ;
define adm_ann/across "Ann�e d'administration"  order=data;
define ucd7/group format=$ucd. "Code UCD" order=data;
define nb_patient/ "Nb patients" format=commaxa.;
define nb_adm/ "Nb administr�" format=commaxd.;
define nb_del/ "Nb de d�livrances" format=commaxa.;
Title1 "TRODELVY administr� lors d'un s�jour hospitalier dans un ES priv�  (donn�es DCIR)";
Title2 "Date de traitement: &datetrt";
run;

proc report data=out_pmsi_trodelvy_ace nowd  split='*' missing; 
columns ucd7 adm_ann, (nb_patient nb_adm nb_del) ;
define adm_ann/across "Ann�e d'administration"  order=data;
define ucd7/group format=$ucd. "Code UCD" order=data;
define nb_patient/ "Nb patients" format=commaxa.;
define nb_adm/ "Nb administr�" format=commaxd.;
define nb_del/ "Nb de d�livrances" format=commaxa.;
Title1 "TRODELVY r�troc�d� dans un ES public (donn�es PMSI)";
Title2 "Date de traitement: &datetrt";
run;

proc report data=out_dcir_trodelvy_retro nowd  split='*' missing; 
columns ucd7 adm_ann, (nb_patient nb_adm nb_del) ;
define adm_ann/across "Ann�e d'administration"  order=data;
define ucd7/group format=$ucd. "Code UCD" order=data;
define nb_patient/ "Nb patients" format=commaxa.;
define nb_adm/ "Nb administr�" format=commaxd.;
define nb_del/ "Nb de d�livrances" format=commaxa.;
Title1 "TRODELVY r�troc�d� dans un ES priv� ou public (donn�es DCIR)";
Title2 "Date de traitement: &datetrt";
run;

proc report data=out_trodelvy nowd  split='*' missing; 
columns ucd7 ucd_top_ucd adm_ann, (nb_patient nb_adm nb_del) ;
define adm_ann/across "Ann�e d'administration"  order=data;
define ucd7/group format=$ucd. "Code UCD" order=data;
define ucd_top_ucd/group format=topucd. "A0"X order=data;
define nb_patient/ "Nb patients" format=commaxa.;
define nb_adm/ "Nb administr�" format=commaxd.;
define nb_del/ "Nb de d�livrances" format=commaxa.;
Title1 "TRODELVY r�troc�d� ou administr� dans un ES priv� ou public (donn�es DCIR et PMSI)";
Title2 "Date de traitement: &datetrt";
run;

/**************SCEMBLIX****************/
ods excel options(start_at="B2" embedded_titles="yes" embedded_footnotes='yes' sheet_name="SCEMBLIX" sheet_interval='now' );

proc report data=out_pmsi_scemblix nowd  split='*' missing; 
columns ucd7 adm_ann, (nb_patient nb_adm nb_del) ;
define adm_ann/across "Ann�e d'administration"  order=data;
define ucd7/group format=$ucd. "Code UCD" order=data;
define nb_patient/ "Nb patients" format=commaxa.;
define nb_adm/ "Nb administr�" format=commaxd.;
define nb_del/ "Nb de d�livrances" format=commaxa.;
Title1 "SCEMBLIX administr� lors d'un s�jour hospitalier dans un ES public (donn�es PMSI)";
Title2 "Date de traitement: &datetrt";
run;

proc report data=out_pmsi_scemblix_oqn nowd  split='*' missing; 
columns ucd7 adm_ann, (nb_patient nb_adm nb_del) ;
define adm_ann/across "Ann�e d'administration"  order=data;
define ucd7/group format=$ucd. "Code UCD" order=data;
define nb_patient/ "Nb patients" format=commaxa.;
define nb_adm/ "Nb administr�" format=commaxd.;
define nb_del/ "Nb de d�livrances" format=commaxa.;
Title1 "SCEMBLIX administr� lors d'un s�jour hospitalier dans un ES priv� (donn�es PMSI)";
Title2 "Date de traitement: &datetrt";
run;

proc report data=out_dcir_scemblix_hospit nowd  split='*' missing; 
columns ucd7 adm_ann, (nb_patient nb_adm nb_del) ;
define adm_ann/across "Ann�e d'administration"  order=data;
define ucd7/group format=$ucd. "Code UCD" order=data;
define nb_patient/ "Nb patients" format=commaxa.;
define nb_adm/ "Nb administr�" format=commaxd.;
define nb_del/ "Nb de d�livrances" format=commaxa.;
Title1 "SCEMBLIX administr� lors d'un s�jour hospitalier dans un ES priv� (donn�es DCIR)";
Title2 "Date de traitement: &datetrt";
run;

proc report data=out_pmsi_scemblix_ace nowd  split='*' missing; 
columns ucd7 adm_ann, (nb_patient nb_adm nb_del) ;
define adm_ann/across "Ann�e d'administration"  order=data;
define ucd7/group format=$ucd. "Code UCD" order=data;
define nb_patient/ "Nb patients" format=commaxa.;
define nb_adm/ "Nb administr�" format=commaxd.;
define nb_del/ "Nb de d�livrances" format=commaxa.;
Title1 "SCEMBLIX r�troc�d� dans un ES public (donn�es PMSI)";
Title2 "Date de traitement: &datetrt";
run;

proc report data=out_dcir_scemblix_retro nowd  split='*' missing; 
columns ucd7 adm_ann, (nb_patient nb_adm nb_del) ;
define adm_ann/across "Ann�e d'administration"  order=data;
define ucd7/group format=$ucd. "Code UCD" order=data;
define nb_patient/ "Nb patients" format=commaxa.;
define nb_adm/ "Nb administr�" format=commaxd.;
define nb_del/ "Nb de d�livrances" format=commaxa.;
Title1 "SCEMBLIX r�troc�d� dans un ES priv� ou public (donn�es DCIR)";
Title2 "Date de traitement: &datetrt";
run;

proc report data=out_scemblix nowd  split='*' missing; 
columns ucd7 ucd_top_ucd adm_ann , (nb_patient nb_adm nb_del) ;
define adm_ann/across "Ann�e d'administration"  order=data;
define ucd7/group format=$ucd. "Code UCD" order=data;
define ucd_top_ucd/group format=topucd. "A0"X order=data;
define nb_patient/ "Nb patients" format=commaxa.;
define nb_adm/ "Nb administr�" format=commaxd.;
define nb_del/ "Nb de d�livrances" format=commaxa.;
Title1 "SCEMBLIX r�troc�d� ou administr� dans un ES priv� ou public (donn�es DCIR et PMSI)";
Title2 "Date de traitement: &datetrt";
run;


ods excel close;
/******************************************SUPPRESSION DES TABLES CREEES DANS ORAUSER**********************************************/
proc sql;
  drop table orauser.extract_ap_ben;
quit;

 
