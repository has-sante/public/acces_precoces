---
output: 
  rmarkdown::html_vignette
title: "Exemple de rapport"

params:
  nom_medicament: "Keytruda"
  ucd_codes_list: NULL
  code_indication: "CPEMB03"
  indications_multiples: FALSE
  force_extraction: FALSE
  start_date: "2022-04-12"
  end_date: "2022-08-26"
---

## Vision géographique

A venir

```{r, run = FALSE, include=FALSE}
# library(sf)

# # Extract department code from the first two characters of adresse_code_postal
# consos_clean$department <- substr(consos_clean$adresse_code_postal, 1, 2)

# # Create a data frame with the total number of administrations by department
# total_by_department <- aggregate(nb_dispensations ~ department, data = df, sum)

# # Load the shapefile for French departments (make sure to download the appropriate shapefile)
# # You can find the shapefile here: https://www.data.gouv.fr/fr/datasets/contours-des-departements-francais-issus-d-openstreetmap/
# shapefile_path <- file.path(PATH2DATA, "referentiels", "departements-20180101.shp")
# departments_sf <- st_read(shapefile_path, stringsAsFactors = FALSE)

# # Merge the total_by_department data with the departments_sf data
# departments_map <- merge(departments_sf, total_by_department, by.x = "code_insee", by.y = "department")

# # Create the static map with ggplot2
# ggplot() +
#     geom_sf(data = departments_map, aes(fill = nb_dispensations)) +
#     scale_fill_viridis_c() +
#     labs(title = "Total Number of Administrations by Department") +
#     theme_minimal()
```
